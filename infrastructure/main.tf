/* -------------------------------------------------------------------------- */
/*                     Terraform state with gitlab backend                    */
/* -------------------------------------------------------------------------- */

terraform {
  backend "http" {
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5

  }
}

/* -------------------------------------------------------------------------- */
/*                                     AWS                                    */
/* -------------------------------------------------------------------------- */

data "aws_region" "current" {}
data "azurerm_client_config" "current" {}

locals {
  heroku = {
    region = "eu"
  }
  app = {
    admin_email        = "admin@tlnk.fr"
    admin_username     = "admin"
    name               = "service-catalog"
    domain             = "tlnk.fr"
    azure_zone_rg_name = "TF_PROD_DNS_PUBLIC"
    callback_url       = "accounts/microsoft/login/callback/"

    prod = {
      heroku_formation_size = "free"
      dns_record_name       = "api"
      ses_domain            = ""
      email                 = "no-reply"


    }

    stage = {
      heroku_formation_size = "free"
      dns_record_name       = "api.stage"
      ses_domain            = "stage."
      email                 = "no-reply"
    }
  }
}


resource "aws_s3_bucket" "storage_bucket" {
  bucket = "tf-${local.environment}-${local.app.name}"
  acl    = "private"


  versioning {
    enabled = true
  }

  tags = {
    terraform   = "TRUE"
    automatique = "TRUE"
    application = "${local.app.name}"
    environment = "${local.environment}"
  }
}

resource "aws_s3_bucket_public_access_block" "storage_bucket_block_public_access" {
  bucket = aws_s3_bucket.storage_bucket.id

  block_public_acls       = false
  block_public_policy     = false
  ignore_public_acls      = false
  restrict_public_buckets = false

}

resource "aws_iam_user" "app_svc" {
  name = "tf-svc-${local.environment}-${local.app.name}"

  tags = {
    terraform   = "TRUE"
    automatique = "TRUE"
    application = "${local.app.name}"
    environment = "${local.environment}"
  }
}

resource "aws_iam_access_key" "app_access_key" {
  user = aws_iam_user.app_svc.name
}

resource "aws_iam_group" "app_s3_full_access_group" {
  name = "tf-s3-full-access-${local.environment}-${local.app.name}"
}


resource "aws_iam_group_policy" "pm_policy_s3_full_access" {
  name  = "tf-s3-full-access-${local.environment}-${local.app.name}"
  group = aws_iam_group.app_s3_full_access_group.name

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Sid" : "VisualEditor0",
        "Effect" : "Allow",
        "Action" : [
          "s3:ListStorageLensConfigurations",
          "s3:ListAccessPointsForObjectLambda",
          "s3:GetAccessPoint",
          "s3:PutAccountPublicAccessBlock",
          "s3:GetAccountPublicAccessBlock",
          "s3:ListAllMyBuckets",
          "s3:ListAccessPoints",
          "s3:PutAccessPointPublicAccessBlock",
          "s3:ListJobs",
          "s3:PutStorageLensConfiguration",
          "s3:ListMultiRegionAccessPoints",
          "s3:CreateJob"
        ],
        "Resource" : "*"
      },
      {
        "Sid" : "VisualEditor1",
        "Effect" : "Allow",
        "Action" : "s3:*",
        "Resource" : ["${aws_s3_bucket.storage_bucket.arn}", "${aws_s3_bucket.storage_bucket.arn}/*"]
      }
    ]
  })
}

resource "aws_iam_group_membership" "pm_app_group_membership" {
  name = "tf-group-membership-${local.environment}-${local.app.name}"

  users = [
    aws_iam_user.app_svc.name,
  ]

  group = aws_iam_group.app_s3_full_access_group.name
}

resource "random_password" "django_secret" {
  length  = 48
  special = false
}

resource "random_password" "django_admin_pwd" {
  length  = 20
  special = false
}

/* -------------------------------------------------------------------------- */
/*                                   Heroku                                   */
/* -------------------------------------------------------------------------- */

resource "heroku_app" "app_app_api" {
  name   = "${local.environment}-${local.app.name}"
  region = local.heroku.region

}

resource "heroku_app_config_association" "app_app_api" {
  app_id = heroku_app.app_app_api.id
  sensitive_vars = {
    DJANO_SECRET_KEY              = random_password.django_secret.result
    DJANGO_SUPERUSER_EMAIL        = local.app.admin_email
    DJANGO_SUPERUSER_PASSWORD     = random_password.django_admin_pwd.result
    AWS_S3_BUCKET_ARN             = aws_s3_bucket.storage_bucket.arn
    AWS_S3_BUCKET_NAME            = aws_s3_bucket.storage_bucket.bucket
    AWS_ACCESS_KEY_ID             = aws_iam_access_key.app_access_key.id
    AWS_ACCESS_KEY_SECRET         = aws_iam_access_key.app_access_key.secret
    AWS_S3_REGION_NAME            = data.aws_region.current.name
    USE_AWS_S3_FOR_STATICFILES    = "true"
    ENV                           = local.environment
    DB_NAME                       = "${local.environment}-service-catalog"
    DB_PASSWD                     = ""
    DB_PORT                       = ""
    DB_SERVER                     = ""
    DB_USER                       = "${local.environment}-service-catalog"
    SL_AZURE_TENANT_ID            = data.azurerm_client_config.current.tenant_id
    CSRF_TRUSTED_ORIGINS          = trimsuffix(heroku_app.app_app_api.web_url, "/")
    ACCOUNT_DEFAULT_HTTP_PROTOCOL = "https"
    AZURE_APP_ID                  = azuread_application.azure_app.application_id
    AZURE_APP_SECRET              = azuread_application_password.azure_app_secret.value
    LOAD_HEROKU_CONFIG            = "true"
    SERVER_BIND_IP                = "0.0.0.0"

  }
}

resource "heroku_formation" "app_formation" {
  count    = local.app[local.environment].heroku_formation_size == "free" ? 0 : 1
  app      = heroku_app.app_app_api.name
  type     = "web"
  size     = local.app[local.environment].heroku_formation_size
  quantity = 1
}


resource "heroku_domain" "app_custom_domain" {
  app      = heroku_app.app_app_api.name
  hostname = "${local.app[local.environment].dns_record_name}.${local.app.domain}"
}

/* -------------------------------------------------------------------------- */
/*                                     DNS                                    */
/* -------------------------------------------------------------------------- */

resource "azurerm_dns_cname_record" "app_app_record" {
  name                = local.app[local.environment].dns_record_name
  zone_name           = local.app.domain
  resource_group_name = local.app.azure_zone_rg_name
  ttl                 = 300
  record              = heroku_domain.app_custom_domain.cname
}

/* -------------------------------------------------------------------------- */
/*                                    Azure                                   */
/* -------------------------------------------------------------------------- */

resource "azuread_application" "azure_app" {
  display_name     = "${local.environment}-${local.app.name}"
  sign_in_audience = "AzureADMyOrg"

  web {
    homepage_url  = heroku_app.app_app_api.web_url
    redirect_uris = ["https://${heroku_domain.app_custom_domain.hostname}/${local.app.callback_url}", "${heroku_app.app_app_api.web_url}${local.app.callback_url}"]

    implicit_grant {
      access_token_issuance_enabled = true
      id_token_issuance_enabled     = true
    }
  }
}

resource "azuread_application_password" "azure_app_secret" {
  application_object_id = azuread_application.azure_app.object_id
}
