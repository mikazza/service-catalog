FROM python:3.10.13-slim
LABEL maintainer="tle@tlnk.fr"

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV ACCEPT_EULA=Y
ARG VERSION
ENV APP_VERSION=${VERSION}

WORKDIR /app

RUN apt-get update && \
    apt-get install --no-install-recommends -y nano libpq-dev python3-dev gcc default-libmysqlclient-dev build-essential curl redis-server

RUN curl https://packages.microsoft.com/keys/microsoft.asc -o /etc/apt/trusted.gpg.d/microsoft.asc && \
    curl https://packages.microsoft.com/config/debian/10/prod.list -o /etc/apt/sources.list.d/mssql-release.list && \
    apt-get update && \
    apt-get install -y msodbcsql17

COPY /src/poetry.lock /app/poetry.lock
COPY /src/pyproject.toml /app/pyproject.toml

RUN pip install --upgrade pip setuptools wheel poetry
RUN poetry config virtualenvs.create false && poetry install --no-interaction --no-dev

COPY /entrypoint /entrypoint

RUN chmod a+x /entrypoint/entrypoint.sh && \
    chmod a+x /entrypoint/entrypoint.d/*.sh

COPY /src /app

RUN apt-get remove --purge -y libpq-dev python3-dev gcc default-libmysqlclient-dev build-essential && \
    apt-get clean && rm -rf /var/lib/apt/lists/*

ENTRYPOINT ["/bin/bash", "/entrypoint/entrypoint.sh"]

LABEL org.label-schema.name="tlnk.fr"
LABEL org.label-schema.description="service catalog"
LABEL org.label-schema.url="https://www.tlnk.fr"
LABEL org.label-schema.vendor="TLNK"
LABEL org.label-schema.vcs-ref=$VCS_REF
LABEL org.label-schema.vcs-url="https://gitlab.com/tle06/service-catalog"
LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.version=$BUILD_VERSION
LABEL org.label-schema.build-date=$BUILD_DATE
LABEL org.label-schema.docker.cmd="docker run registry.gitlab.com/tle06/service-catalog"
