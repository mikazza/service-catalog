Provider
========

Provider refer to the entity that propose the service. It can be internal to your company or external.
- A service can refer a provider
- A contract can refer a provider 

Composition of a provider
-------------------------

.. csv-table::
   :widths: auto
   :header: "Field", "Description", "Mandatory", "Default value"

   "Name","The name of the provider","Yes"   
   "Email","The address email where you can reach the provider,"No"   
   "Phone","The phone number of the provider","No"   
   "Address","The official address","No"   
   "Street Number","The street number in the official address","No"   
   "Zip Code","The zip code for the address","No"   
   "City","The city link to the address","No"   
   "Country","The country of the Address","No"   
   "Currency preffered","The preffered currency of the provider","No","EUR"
   "VAT Number","The VAT number of the provider","No"   

