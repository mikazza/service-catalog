Hardware
=======

An hardware object represente a physical device that you belong to your catalogue. for example, a laptop, a printer, a smartphone, a screen,...

Composition of an hardware object
---------------------------------

.. csv-table::
   :widths: auto
   :header: "Field", "Description", "Mandatory", "Deault value"

   "Name","Name of the hardware","Yes"
   "Description","The descriptino of the hardware","Yes"
   "Status","The status of the hardware","Yes","Active"
   "Hardware category","Category of the hardware","No"
   "Provider","Who is the provider","No"
   "Category","Category where the hardware will be assigned","No"
   "Model name","the model name","No"
   "Model number","The model number of the hardware","No"
   "Support URL","URL that will redirect to the support website or portal","No"
   "Created by","The creator of the hardware","Yes","User who created the hardware"


hardware category
----------------

hardware can be classify with custom category.
Category can be created by an Admin from the admin web page.

|diagram|

.. |diagram| image:: https://orhanergun.net/wp-content/uploads/2019/11/tiers.jpg
                  :target: https://orhanergun.net/wp-content/uploads/2019/11/tiers.jpg


hardware Status
--------------

.. csv-table::
   :widths: auto
   :header: "Status", "Description"

   "Active","hardware is currently in used"
   "Decommission","hardware is not used anymore or decomission"
