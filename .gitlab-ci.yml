# ---------------------------------------------------------------------------- #
#                             Default configuration                            #
# ---------------------------------------------------------------------------- #

default:
  tags:
    - gitlab-org-docker

include:
  - local: "gitlab-ci/variables/terraform.gitlab-ci.yml"
  - remote: "https://gitlab.com/tle06/gitlab-ci/-/raw/v1.0.5/jobs/kaniko-build.gitlab-ci.yml"
  - remote: "https://gitlab.com/tle06/gitlab-ci/-/raw/v1.0.5/jobs/heroku-deploy.gitlab-ci.yml"

# ---------------------------------------------------------------------------- #
#                                     stage                                    #
# ---------------------------------------------------------------------------- #

stages:
  - infra
  - build
  - deploy

# ---------------------------------------------------------------------------- #
#                                     Rules                                    #
# ---------------------------------------------------------------------------- #

.docker-rules:
  rules:
    - if: '$CI_COMMIT_BRANCH == "main"'
      variables:
        ENVIRONMENT: stage
        GIT_BRANCH: main
        CONTAINER_NAME: "stage-${CI_COMMIT_SHORT_SHA}"
      changes:
        - Dockerfile
        - src/**/*
        - entrypoint/**/*
    - if: '$CI_COMMIT_TAG =~ /^v[0-9]+\.[0-9]+\.[0-9]+$/'
      variables:
        ENVIRONMENT: prod
        GIT_BRANCH: main
        CONTAINER_NAME: $CI_COMMIT_TAG
        K_BUILD_ARG: "--destination ${CI_REGISTRY_IMAGE}:latest"
    - if: '$CI_COMMIT_TAG =~ /^dev-v[0-9]+\.[0-9]+\.[0-9]+$/'
      variables:
        ENVIRONMENT: stage
        GIT_BRANCH: main
        CONTAINER_NAME: $CI_COMMIT_TAG

.infra-rules:
  rules:
    - if: '($CI_COMMIT_BRANCH == "main" && $SKIP_INFRA == null)'
      when: on_success
      variables:
        ENVIRONMENT: stage
        GIT_BRANCH: main
      changes:
        - ${TF_ROOT_FOLDER_NAME}/**/*
    - if: '($CI_COMMIT_BRANCH == "main" && $SKIP_INFRA == null) || $CI_COMMIT_TAG =~ /^v[0-9]+\.[0-9]+\.[0-9]+$/'
      when: on_success
      variables:
        ENVIRONMENT: prod
        GIT_BRANCH: main
      changes:
        - ${TF_ROOT_FOLDER_NAME}/**/*
    - if: '($CI_COMMIT_BRANCH == "main" && $SKIP_INFRA == null) || $CI_COMMIT_TAG =~ /^dev-v[0-9]+\.[0-9]+\.[0-9]+$/'
      when: on_success
      variables:
        ENVIRONMENT: stage
        GIT_BRANCH: main
      changes:
        - ${TF_ROOT_FOLDER_NAME}/**/*

# ---------------------------------------------------------------------------- #
#                                     jobs                                     #
# ---------------------------------------------------------------------------- #
deploy-tf:
  extends:
    - .infra-rules
  variables:
    TF_ROOT: $TF_ROOT_FOLDER_NAME
  stage: infra
  trigger:
    include: "/gitlab-ci/pipelines/tf-pipeline.gitlab-ci.yml"
    strategy: depend

build-container:
  extends:
    - .docker-rules
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor
      --context $CI_PROJECT_DIR
      --dockerfile $CI_PROJECT_DIR/Dockerfile
      --build-arg VERSION=${CONTAINER_NAME}
      --destination "${CI_REGISTRY_IMAGE}:${CONTAINER_NAME}"
      --tarPath image.tar
      --cache=true
      --cache-copy-layers=true
  when: on_success
  artifacts:
    paths:
      - image.tar

publish-gitlab:
  extends:
    - .docker-rules
  stage: deploy
  image:
    name: gcr.io/go-containerregistry/crane:debug
    entrypoint: [""]
  script:
    - crane auth login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - crane push image.tar "${CI_REGISTRY_IMAGE}:${CONTAINER_NAME}"
  needs:
    - build-container

publish-docker-hub:
  extends:
    - .docker-rules
  stage: deploy
  image:
    name: gcr.io/go-containerregistry/crane:debug
    entrypoint: [""]
  script:
    - crane auth login -u $DOCKER_HUB_USERNAME -p $DOCKER_HUB_PASSWORD index.docker.io
    - crane push image.tar "index.docker.io/tlnk/service-catalog:${CONTAINER_NAME}"
  needs:
    - build-container

publish-heroku:
  extends:
    - .docker-rules
  stage: deploy
  image:
    name: gcr.io/go-containerregistry/crane:debug
    entrypoint: [""]
  script:
    - crane auth login registry.heroku.com -u $HEROKU_EMAIL -p $HEROKU_TOKEN
    - crane push image.tar registry.heroku.com/${ENVIRONMENT}-service-catalog/web
  needs:
    - build-container

deploy-heroku:
  image: ubuntu:latest
  extends:
    - .docker-rules
    - .deploy-heroku
  stage: deploy
  variables:
    HEROKU_EMAIL: $HEROKU_EMAIL
    HEROKU_API_KEY: $HEROKU_API_KEY
    APP_NAME: $ENVIRONMENT-service-catalog
    HEROKU_WORHER_TYPE: web
  needs:
    - publish-heroku
  when: on_success
