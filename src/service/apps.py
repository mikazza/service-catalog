from django.apps import AppConfig
from health_check.plugins import plugin_dir


class ServiceConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "service"
    verbose_name = "service"
    label = "service"

    def ready(self):
        from django.conf import settings
        from service.models import IdentityProvider, User
        import logging
        from service.healthcheck import AccessControlDatabaseCheck

        plugin_dir.register(AccessControlDatabaseCheck)
        logger = logging.getLogger(__name__)
        idp_name = None
        if settings.IDP_PROVIDER_NAME:
            try:
                idp_name = IdentityProvider.objects.filter(
                    name=settings.IDP_PROVIDER_NAME.lower()
                ).first()
            except Exception as e:
                logger.error(msg=e)

            if not idp_name:
                logger.info(msg=f"IDP {settings.IDP_PROVIDER_NAME} do not exist")
                try:
                    new_idp = IdentityProvider(
                        name=settings.IDP_PROVIDER_NAME.lower(),
                        createdByUserId=User.objects.filter(pk=1).first(),
                    )
                    new_idp.save()
                    logger.info(msg=f"IDP created for {new_idp.name}")
                except Exception as e:
                    logger.error(msg=e)
            else:
                logger.info(msg=f"IDP already exist for {idp_name.name}")
