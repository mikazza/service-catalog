from health_check.backends import BaseHealthCheckBackend
from django.db import connections
from health_check.exceptions import HealthCheckException
from timeit import default_timer as timer
import logging

logger = logging.getLogger("health-check")


class AccessControlDatabaseCheck(BaseHealthCheckBackend):
    #: The status endpoints will respond with a 200 status code
    #: even if the check errors.
    critical_service = False

    def check_status(self):
        start = timer()
        try:
            connection = connections["acccess_control"]
            connection.connect()
            return True
        except HealthCheckException as e:
            self.add_error(e, e)
        except BaseException as e:
            logger.exception("Unexpected Error!")
            self.add_error(e, e)
        finally:
            self.time_taken = timer() - start

    def identifier(self):
        return self.__class__.__name__  # Display name on the endpoint.
