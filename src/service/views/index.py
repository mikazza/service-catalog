from django.views.generic import RedirectView, TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin


class IndexRedirectView(RedirectView):
    url = "service/"


class IndexView(LoginRequiredMixin, TemplateView):
    template_name = "index/index.html"
