from service.models import Contract, ContractDocument
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
    View,
)
from django.urls import reverse_lazy
from core.utils import get_pagination_configuration
from service.forms import UploadContractDocumentFrom
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.db.models import Q
from import_export import resources
from core.utils import ViewExport


class ContractListView(PermissionRequiredMixin, ListView):
    model = Contract
    context_object_name = "instance_object"
    template_name = "contract/contracts.html"
    extra_context = {
        "custom_title": "Contracts",
        "create_url": "contract_creation",
        "update_url": "contract_update",
        "details_url": "contract_details",
        "delete_url": "contract_delete",
        "export_url": "contract_export",
    }
    permission_required = "service.view_contract"


class ContractSearchView(PermissionRequiredMixin, ListView):
    model = Contract
    context_object_name = "instance_object"
    template_name = "contract/contracts.html"
    extra_context = {
        "custom_title": "Contracts",
        "create_url": "contract_creation",
        "update_url": "contract_update",
        "details_url": "contract_details",
        "delete_url": "contract_delete",
        "export_url": "contract_export",
    }
    permission_required = "service.view_contract"

    def get_queryset(self):
        query = self.request.GET.get("query")
        object_list = Contract.objects.filter(
            Q(name__icontains=query) | Q(provider__name__icontains=query)
        )
        return object_list

    def get_success_url(self):
        return reverse_lazy(
            "pricing_list", kwargs={"query": self.request.GET.get("query")}
        )


class ContractDetailView(PermissionRequiredMixin, DetailView):
    model = Contract
    template_name = "contract/contract.html"
    context_object_name = "instance_object"
    extra_context = {
        "custom_title": "Contract",
        "update_url": "contract_update",
        "delete_url": "contract_delete",
        "list_url": "contract_list",
    }
    permission_required = "service.view_contract"

    def get_success_url(self):
        tab = self.request.GET.get("tab")
        path = reverse_lazy("contract_details", kwargs={"pk": self.object.pk})
        tab = self.request.GET.get("tab", "main")
        url = f"{path}?tab={tab}"
        return url


class ContractCreateView(PermissionRequiredMixin, CreateView):
    model = Contract
    template_name = "contract/create_update_contract.html"
    extra_context = {
        "form_action": "create",
        "custom_title": "Contract",
        "list_url": "contract_list",
    }
    context_object_name = "instance_object"
    permission_required = "service.add_contract"
    fields = [
        "name",
        "provider",
        "start_date",
        "end_date",
        "renewal_date",
        "duration",
        "duration_unit",
        "total_cost",
        "cost_unit",
        "volume",
        "currency",
        "status",
        "company",
    ]

    def form_valid(self, form):
        form.instance.createdByUserId = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy("contract_details", kwargs={"pk": self.object.pk})


class ContractUpdateView(PermissionRequiredMixin, UpdateView):
    model = Contract
    template_name = "contract/create_update_contract.html"
    extra_context = {
        "form_action": "update",
        "custom_title": "Contract",
        "list_url": "contract_list",
    }
    context_object_name = "instance_object"
    permission_required = "service.change_contract"
    fields = [
        "name",
        "provider",
        "start_date",
        "end_date",
        "renewal_date",
        "duration",
        "duration_unit",
        "total_cost",
        "cost_unit",
        "volume",
        "currency",
        "status",
        "company",
    ]

    def form_valid(self, form):
        form.instance.createdByUserId = self.request.user

        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy("contract_details", kwargs={"pk": self.object.pk})


class ContractDeleteView(PermissionRequiredMixin, DeleteView):
    model = Contract
    template_name = "generic/views/confirm_delete.html"
    extra_context = {"form_action": "delete"}
    permission_required = "service.delete_contract"

    def get_success_url(self):
        return reverse_lazy("contract_list")


class ContractDocumentCreateView(PermissionRequiredMixin, View):
    form_class = UploadContractDocumentFrom
    permission_required = "service.add_contractdocument"

    def post(self, request, pk):
        form = self.form_class(request.POST, request.FILES)

        if form.is_valid():
            filename = request.FILES["file"].name
            form.instance.name = filename
            form.instance.createdByUserId = self.request.user
            form.instance.lpk = pk
            result = form.save()
            contract = get_object_or_404(Contract, pk=pk)
            contract.document.add(result.id)
            tab = self.request.GET.get("tab")
            path = reverse_lazy("contract_details", kwargs={"pk": pk})
            tab = self.request.GET.get("tab", "document")
            url = f"{path}?tab={tab}"

        return HttpResponseRedirect(url)


class ContractDocumentRemoveView(PermissionRequiredMixin, View):
    permission_required = "service.delete_contractdocument"

    def post(self, request, pk, lpk):
        request.POST.get("relation")
        curent_object = get_object_or_404(Contract, pk=pk)
        curent_object.document.remove(lpk)

        link_object = get_object_or_404(ContractDocument, pk=lpk)
        link_object.delete()

        tab = self.request.GET.get("tab")
        path = reverse_lazy("contract_details", kwargs={"pk": pk})
        tab = self.request.GET.get("tab", "document")
        url = f"{path}?tab={tab}"

        return HttpResponseRedirect(url)


class ContractResource(resources.ModelResource):
    class Meta:
        model = Contract


class ContractExport(PermissionRequiredMixin, ViewExport):
    filename = "contract.csv"
    content_type = "csv"
    resource = ContractResource
    permission_required = "service.export_contract"
