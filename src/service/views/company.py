from service.models import Company
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
)
from django.urls import reverse_lazy
from django.db.models import Q
from import_export import resources
from core.utils import ViewExport


class CompanyListView(LoginRequiredMixin, ListView):
    model = Company
    template_name = "company/companies.html"
    context_object_name = "instance_object"
    extra_context = {
        "custom_title": "Companies",
        "create_url": "company_creation",
        "update_url": "company_update",
        "details_url": "company_details",
        "delete_url": "company_delete",
        "export_url": "company_export",
    }


class CompanySearchView(LoginRequiredMixin, ListView):
    model = Company
    context_object_name = "instance_object"
    template_name = "company/companies.html"
    extra_context = {
        "custom_title": "Companies",
        "create_url": "company_creation",
        "update_url": "company_update",
        "details_url": "company_details",
        "delete_url": "company_delete",
        "export_url": "company_export",
    }

    def get_queryset(self):
        query = self.request.GET.get("query")
        object_list = Company.objects.filter(
            Q(name__icontains=query)
            | Q(invoice_email__icontains=query)
            | Q(vat_number__icontains=query)
            | Q(address__icontains=query)
            | Q(country__icontains=query)
            | Q(city__icontains=query)
        )
        return object_list

    def get_success_url(self):
        return reverse_lazy(
            "company_list", kwargs={"query": self.request.GET.get("query")}
        )


class CompanyDetailView(LoginRequiredMixin, DetailView):
    model = Company
    context_object_name = "instance_object"
    template_name = "company/company.html"
    extra_context = {
        "custom_title": "Company",
        "update_url": "company_update",
        "delete_url": "company_delete",
        "list_url": "company_list",
    }

    def get_success_url(self):
        tab = self.request.GET.get("tab")
        path = reverse_lazy("company_details", kwargs={"pk": self.object.pk})
        tab = self.request.GET.get("tab", "main")
        url = f"{path}?tab={tab}"
        return url


class CompanyCreateView(PermissionRequiredMixin, CreateView):
    model = Company
    template_name = "company/create_update_company.html"
    permission_required = "service.add_company"
    extra_context = {
        "form_action": "create",
        "custom_title": "Company",
        "list_url": "company_list",
    }
    context_object_name = "instance_object"
    fields = [
        "name",
        "vat_number",
        "invoice_email",
        "address",
        "street_number",
        "city",
        "zip_code",
        "country",
        "brand_name",
        "activity",
        "incorporation_date",
        "share_capital",
        "tax_id",
        "trade_register",
        "shareholders",
    ]

    def form_valid(self, form):
        form.instance.createdByUserId = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy("company_details", kwargs={"pk": self.object.pk})


class CompanyUpdateView(PermissionRequiredMixin, UpdateView):
    model = Company
    permission_required = "service.change_company"
    template_name = "company/create_update_company.html"
    extra_context = {
        "form_action": "update",
        "custom_title": "Company",
        "list_url": "company_list",
    }
    context_object_name = "instance_object"
    fields = [
        "vat_number",
        "invoice_email",
        "address",
        "street_number",
        "city",
        "zip_code",
        "country",
        "brand_name",
        "activity",
        "incorporation_date",
        "share_capital",
        "tax_id",
        "trade_register",
        "shareholders",
    ]

    def get_success_url(self):
        return reverse_lazy("company_details", kwargs={"pk": self.object.pk})


class CompanyDeleteView(PermissionRequiredMixin, DeleteView):
    model = Company
    template_name = "generic/views/confirm_delete.html"
    extra_context = {"form_action": "delete"}
    permission_required = "service.delete_company"

    def get_success_url(self):
        return reverse_lazy("company_list")


class CompanyResource(resources.ModelResource):
    class Meta:
        model = Company


class CompanyExport(PermissionRequiredMixin, ViewExport):
    filename = "company.csv"
    content_type = "csv"
    resource = CompanyResource
    permission_required = "service.export_company"
