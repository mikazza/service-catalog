from service.models import Contact, Provider
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
    View,
)
from django.urls import reverse_lazy
from core.utils import get_pagination_configuration
from django.shortcuts import get_object_or_404
from django.db.models import Q
from import_export import resources
from core.utils import ViewExport
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.http import HttpResponseRedirect


class ContactListView(LoginRequiredMixin, ListView):
    model = Contact
    template_name = "contact/contacts.html"
    context_object_name = "instance_object"
    extra_context = {
        "custom_title": "Contacts",
        "create_url": "contact_creation",
        "update_url": "contact_update",
        "details_url": "contact_details",
        "delete_url": "contact_delete",
        "export_url": "contact_export",
    }


class ContactSearchView(LoginRequiredMixin, ListView):
    model = Contact
    context_object_name = "instance_object"
    template_name = "contact/contacts.html"
    extra_context = {
        "custom_title": "Contacts",
        "create_url": "contact_creation",
        "update_url": "contact_update",
        "details_url": "contact_details",
        "delete_url": "contact_delete",
        "export_url": "contact_export",
    }

    def get_queryset(self):
        query = self.request.GET.get("query")
        object_list = Contact.objects.filter(
            Q(name__icontains=query)
            | Q(email__icontains=query)
            | Q(firstname__icontains=query)
            | Q(lastname__icontains=query)
            | Q(phone__icontains=query)
        )
        return object_list

    def get_success_url(self):
        return reverse_lazy(
            "provider_list", kwargs={"query": self.request.GET.get("query")}
        )


class ContactDetailView(LoginRequiredMixin, DetailView):
    model = Contact
    context_object_name = "instance_object"
    template_name = "contact/contact.html"
    extra_context = {
        "custom_title": "Contact",
        "update_url": "contact_update",
        "delete_url": "contact_delete",
        "list_url": "contact_list",
    }


class ContactCreateView(LoginRequiredMixin, CreateView):
    model = Contact
    template_name = "contact/create_update_contact.html"
    extra_context = {
        "form_action": "create",
        "custom_title": "contact",
        "list_url": "contact_list",
    }
    context_object_name = "instance_object"
    fields = [
        "name",
        "firstname",
        "lastname",
        "email",
        "phone",
    ]

    def form_valid(self, form):
        form.instance.createdByUserId = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        current_provider = get_object_or_404(Provider, pk=self.kwargs["pk"])
        current_provider.contact.add(self.object.pk)
        path = reverse_lazy("provider_details", kwargs={"pk": current_provider.pk})
        tab = self.request.GET.get("tab", "contact")
        url = f"{path}?tab={tab}"
        return url


class ContactUpdateView(LoginRequiredMixin, UpdateView):
    model = Contact
    template_name = "contact/create_update_contact.html"
    extra_context = {
        "form_action": "update",
        "custom_title": "Contact",
        "list_url": "contact_list",
    }
    context_object_name = "instance_object"
    fields = [
        "name",
        "firstname",
        "lastname",
        "email",
        "phone",
    ]

    def get_success_url(self):
        return reverse_lazy("contact_details", kwargs={"pk": self.object.pk})


class ContactDeleteView(LoginRequiredMixin, DeleteView):
    model = Contact
    template_name = "generic/views/confirm_delete.html"
    extra_context = {"form_action": "delete"}

    def get_success_url(self):
        return reverse_lazy("contact_list")


class RemoveContactLinkToProviderView(View):
    def post(self, request, pk, lpk):
        request.POST.get("relation")
        current_object = get_object_or_404(Provider, pk=pk)
        current_object.contact.remove(lpk)
        path = reverse_lazy("provider_details", kwargs={"pk": current_object.pk})
        tab = self.request.GET.get("tab", "contact")
        url = f"{path}?tab={tab}"
        return HttpResponseRedirect(url)


class ContactResource(resources.ModelResource):
    class Meta:
        model = Contact


class ContactExport(PermissionRequiredMixin, ViewExport):
    filename = "contact.csv"
    content_type = "csv"
    resource = ContactResource
    permission_required = "service.export_contact"
