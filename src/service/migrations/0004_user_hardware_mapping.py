# Generated by Django 4.0.1 on 2023-11-19 17:20

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('hardware', '0002_initial'),
        ('service', '0003_user_building_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='hardware_mapping',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='hardware.hardwaremapping'),
        ),
    ]
