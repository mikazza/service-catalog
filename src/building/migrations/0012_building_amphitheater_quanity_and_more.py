# Generated by Django 4.0.1 on 2023-12-04 16:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('building', '0011_building_status'),
    ]

    operations = [
        migrations.AddField(
            model_name='building',
            name='amphitheater_quanity',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='building',
            name='meeting_rom_quantity',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='building',
            name='phonebooth_quantity',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
