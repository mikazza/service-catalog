from celery import shared_task
from core.settings import logger
import pytz
from django.db import connections
from building.models import AccessControlRecord, Building, Controller
from service.models import User
from core.utils import dictfetchall


@shared_task
def dump_access_control_record_task():
    def access_control_records_query(start_id=1):
        query = f"""SELECT DISTINCT 
                    r.id as RECORD_ID,
                    replace(replace(u.EMPEMAILEMPRESA,',',''),'&',' ')  AS USER_EMAIL,
                    replace(replace(u.EMPNOM,',',''),'&',' ')  as USER_FIRSTNAME,
	                replace(replace(u.EMPAPELLIDO1,',',''),'&',' ')  as USER_LASTNAME,
                    CASE
                        WHEN u.EMPTIPO=1 THEN 'Internal employee'
                        WHEN u.EMPTIPO=2 THEN 'External employee'
                        WHEN u.EMPTIPO=4 THEN 'Visitor'
                        WHEN u.EMPTIPO=8 THEN 'Client'
                        ELSE 'no type'
                    END as USER_TYPE,
                    replace(Replace(c.CODIGO,',',''),'&',' ') as OFFICE,
                    replace(replace(r.TARJETA_CODE,',',''),'&',' ') as USER_CARD,
                    r.CONTROLLER_ID as CONTROLLER_ID,
                    co.DESCRIPCION as CONTROLLER_NAME,
                    r.UTC_TIME as DATE
                FROM [MoveManager.NET].[dbo].[Marcajes] as r
                    INNER JOIN [MoveManager.NET].[dbo].[Tarjeta] as t ON r.TARJETA_CODE = t.CODIGO
                    JOIN [MoveManager.NET].[dbo].[Controllers] as co ON r.CONTROLLER_ID =co.ID
                    LEFT JOIN [MoveManager.NET].[dbo].[Usuario] as u ON t.USUARIO_ID = u.ID
                    LEFT JOIN [MoveManager.NET].[dbo].[Centros] as c ON r.CENTRO = c.ID
                    WHERE r.id BETWEEN {start_id} and (select max(id) from [MoveManager.NET].[dbo].[Marcajes])
                    ORDER BY r.ID ASC
                    """

        return query

    counter = 0
    user_create = 0
    building_created = 0
    controller_created = 0

    last_record = AccessControlRecord.objects.all().last()
    last_record_id = 0
    if last_record:
        last_record_id = last_record.source_id
    logger.info(f"last source_id: {last_record_id}")
    logger.info(f"start source_id: {last_record_id+1}")

    connection = connections["acccess_control"].cursor()
    raw_query = access_control_records_query(start_id=last_record_id + 1)
    connection.execute(raw_query)
    queryset = dictfetchall(connection)
    logger.info(f"number of records: {len(queryset)}")

    for obj in queryset:
        exist_source_id = AccessControlRecord.objects.filter(
            source_id=obj.get("RECORD_ID")
        ).first()

        if not exist_source_id:
            building = Building.objects.filter(name=obj.get("OFFICE")).first()

            if not building:
                building = Building(
                    name=obj.get("OFFICE"),
                    createdByUserId=User.objects.filter(pk=1).first(),
                )
                building.save()
                building_created = building_created + 1
                logger.info(f"new building created: {building.name}")

            user = User.objects.filter(email=obj.get("USER_EMAIL")).first()

            if not user and obj.get("USER_EMAIL"):
                user = User(
                    email=obj.get("USER_EMAIL"),
                    is_active=True,
                    is_staff=False,
                    is_superuser=False,
                    first_name=obj.get("USER_FIRSTNAME"),
                    last_name=obj.get("USER_LASTNAME"),
                )
                user.save()
                user_create = user_create + 1
                logger.info(f"new user created: {user.email}")

            controller = Controller.objects.filter(
                name=obj.get("CONTROLLER_NAME")
            ).first()

            if not controller:
                controller = Controller(
                    name=obj.get("CONTROLLER_NAME"),
                    external_id=obj.get("CONTROLLER_ID"),
                    createdByUserId=User.objects.filter(pk=1).first(),
                )
                controller.save()
                controller_created = controller_created + 1
                logger.info(f"new controller created: {controller_created}")

            record = AccessControlRecord(
                source_id=obj.get("RECORD_ID"),
                building=building,
                date=obj.get("DATE").replace(tzinfo=pytz.UTC),
                controller=controller,
                user=user,
                card=obj.get("USER_CARD"),
                user_type=obj.get("USER_TYPE"),
            )
            record.save()
            counter = counter + 1
            logger.info(f"source_id added: {record.source_id}")
            logger.info(f"count new user created: {user_create}")
            logger.info(f"count new building created: {building_created}")
            logger.info(f"count new controller created: {controller_created}")
    return counter
