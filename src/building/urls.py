from django.urls import path
from building.views import (
    BuildingCreateView,
    BuildingDeleteView,
    BuildingDetailView,
    BuildingExport,
    BuildingListView,
    BuildingPresenceStatisticView,
    BuildingSearchView,
    BuildingUpdateView,
    OfficePresenceStatisticsExport,
    OfficePresenceStatisticsListView,
    BuildingDocumentCreateView,
    BuildingDocumentRemoveView,
)


urlpatterns = [
    path(
        "office/presence/statistics",
        OfficePresenceStatisticsListView.as_view(),
        name="office_presence_statistics_list",
    ),
    path(
        "office/presence/statistics/export",
        OfficePresenceStatisticsExport.as_view(),
        name="office_presence_statistics_export",
    ),
    path("building/", BuildingListView.as_view(), name="building_list"),
    path("building/<int:pk>", BuildingDetailView.as_view(), name="building_details"),
    path("building/create", BuildingCreateView.as_view(), name="building_creation"),
    path(
        "building/update/<int:pk>", BuildingUpdateView.as_view(), name="building_update"
    ),
    path(
        "building/delete/<int:pk>", BuildingDeleteView.as_view(), name="building_delete"
    ),
    path("building/search/", BuildingSearchView.as_view(), name="building_search"),
    path("building/export/", BuildingExport.as_view(), name="building_export"),
    path(
        "building/<int:pk>/presence/statistic",
        BuildingPresenceStatisticView.as_view(),
        name="building_presence_statistic_list",
    ),
    path(
        "building/update/upload/<int:pk>",
        BuildingDocumentCreateView.as_view(),
        name="building_document_creation",
    ),
    path(
        "building/update/<int:pk>/removedocument/<int:lpk>",
        BuildingDocumentRemoveView.as_view(),
        name="building_remove_document",
    ),
]
