from django.contrib.auth.mixins import PermissionRequiredMixin
from django.db import connections, connection
from core.utils import dictfetchall
from import_export import resources, fields
from core.utils import ViewExport
from django.http import HttpResponse
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
    View,
)
from django.contrib.auth.mixins import LoginRequiredMixin
from building.models import Building, PublicIP
from django.urls import reverse_lazy
from django.db.models import Q
from building.forms import UploadBuildingDocumentFrom
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from service.models import ContractDocument


def access_control_statistics_query(year: None, office: None):
    query = """SET NOCOUNT ON;
        ;with data as (
        SELECT DISTINCT
            replace(replace(u.EMPEMAILEMPRESA,',',''),'&',' ')  AS USER_EMAIL,
            replace(Replace(c.CODIGO,',',''),'&',' ') as OFFICE,
            CAST(r.UTC_TIME as DATE)  as DATE,
            DATEPART(week,r.UTC_TIME) as WEEK_NUMBER,
            YEAR(r.UTC_TIME) as YEAR
        FROM [MoveManager.NET].[dbo].[Marcajes] as r
            INNER JOIN [MoveManager.NET].[dbo].[Tarjeta] as t ON r.TARJETA_CODE = t.CODIGO
            LEFT JOIN [MoveManager.NET].[dbo].[Usuario] as u ON t.USUARIO_ID = u.ID
            LEFT JOIN [MoveManager.NET].[dbo].[Centros] as c ON r.CENTRO = c.ID
            WHERE DATALENGTH(u.EMPEMAILEMPRESA) >0
            --ORDER BY CAST(r.UTC_TIME as DATE) ASC
            )

        SELECT YEAR, DATE,OFFICE,count(USER_EMAIL) as COUNT from data """

    if year or office:
        query = query + "WHERE "

    if year:
        query = query + f"YEAR ={year}"

    if year and office:
        query = query + "AND "

    if office:
        query = query + f"office like '{office}'"

    group_by = """ GROUP BY YEAR,OFFICE,DATE
            ORDER BY YEAR DESC, OFFICE, DATE DESC;"""

    return query + group_by


class OfficePresenceStatisticsListView(PermissionRequiredMixin, ListView):
    template_name = "building/presence_statistics.html"
    permission_required = "building.export_access_control_record"
    context_object_name = "instance_object"
    extra_context = {
        "custom_title": "Statistics",
        "export_url": "office_presence_statistics_export",
    }

    def get_queryset(self):
        year = self.request.GET.get("year") or None
        office = self.request.GET.get("office") or None

        connection = connections["acccess_control"].cursor()
        raw_query = access_control_statistics_query(year=year, office=office)
        connection.execute(raw_query)
        data = dictfetchall(connection)
        return data


class OfficePresenceStatisticsResource(resources.Resource):
    year = fields.Field(attribute="year", column_name="year")
    date = fields.Field(attribute="date", column_name="date")
    office = fields.Field(attribute="office", column_name="office")
    count = fields.Field(attribute="count", column_name="count")

    class Meta:
        skip_unchanged = True

    def __init__(self, office=None, year=None):
        self.filter_office = office
        self.filter_year = year

    def dehydrate_year(self, obj):
        return obj["year"]

    def dehydrate_date(self, obj):
        return obj["date"]

    def dehydrate_office(self, obj):
        return obj["office"]

    def dehydrate_count(self, obj):
        return obj["count"]

    def get_queryset(self):
        """
        Returns a queryset of all objects for this model. Override this if you
        want to limit the returned queryset.
        """
        connection = connections["acccess_control"].cursor()
        raw_query = access_control_statistics_query(
            year=self.filter_year, office=self.filter_office
        )
        connection.execute(raw_query)
        queryset = dictfetchall(connection)
        data = []
        for obj in queryset:
            data.append(
                {
                    "year": obj["YEAR"],
                    "date": obj["DATE"],
                    "office": obj["OFFICE"],
                    "count": obj["COUNT"],
                }
            )
        return data


class OfficePresenceStatisticsExport(PermissionRequiredMixin, ViewExport):
    filename = "access_control_statistics.csv"
    content_type = "csv"
    permission_required = "building.export_access_control_record"
    resource = OfficePresenceStatisticsResource

    def get(self, *args, **kwargs):
        year = self.request.GET.get("year") or None
        office = self.request.GET.get("office") or None

        dataset = self.resource(office=office, year=year).export()
        response = HttpResponse(dataset.csv, content_type=self.content_type)
        response["Content-Disposition"] = f"attachment; filename={self.filename}"
        return response


class BuildingListView(LoginRequiredMixin, ListView):
    model = Building
    context_object_name = "instance_object"
    template_name = "building/buildings.html"
    extra_context = {
        "custom_title": "Building",
        "create_url": "building_creation",
        "update_url": "building_update",
        "details_url": "building_details",
        "delete_url": "building_delete",
        "export_url": "building_export",
        "list_url": "building_list",
    }


class BuildingDetailView(LoginRequiredMixin, DetailView):
    model = Building
    template_name = "building/building.html"
    context_object_name = "instance_object"
    extra_context = {
        "create_url": "building_creation",
        "update_url": "building_update",
        "details_url": "building_details",
        "delete_url": "building_delete",
        "export_url": "building_export",
        "list_url": "building_list",
        "custom_title": "Building",
    }

    def get_success_url(self):
        tab = self.request.GET.get("tab")
        path = reverse_lazy("building_details")
        tab = self.request.GET.get("tab", "main")
        url = f"{path}?tab={tab}"
        return url


class BuildingCreateView(PermissionRequiredMixin, CreateView):
    model = Building
    template_name = "building/create_update_building.html"
    permission_required = "building.add_building"
    extra_context = {
        "form_action": "create",
        "custom_title": "Building",
        "list_url": "building_list",
    }
    context_object_name = "instance_object"
    fields = [
        "name",
        "target_presence",
        "building_manager",
        "it_support",
        "general_manager",
        "culture_embassador",
        "people_director",
        "people_partner",
        "address",
        "street_number",
        "city",
        "zip_code",
        "country",
        "company",
        "capacity",
        "square_meters",
        "reception",
        "type",
        "phone_number",
        "size",
        "renting",
        "start_rending_date",
        "cost_per_month",
        "currency",
        "banner",
        "status",
        "meeting_rom_quantity",
        "phonebooth_quantity",
        "amphitheater_quantity",
    ]

    def form_valid(self, form):
        form.instance.createdByUserId = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy("building_details", kwargs={"pk": self.object.pk})


class BuildingUpdateView(PermissionRequiredMixin, UpdateView):
    model = Building
    template_name = "building/create_update_building.html"
    permission_required = "building.change_building"
    extra_context = {
        "form_action": "update",
        "custom_title": "Building",
        "list_url": "building_list",
    }
    context_object_name = "instance_object"
    fields = [
        "target_presence",
        "building_manager",
        "it_support",
        "general_manager",
        "culture_embassador",
        "people_director",
        "people_partner",
        "address",
        "street_number",
        "city",
        "zip_code",
        "country",
        "company",
        "capacity",
        "square_meters",
        "reception",
        "type",
        "phone_number",
        "size",
        "renting",
        "start_rending_date",
        "cost_per_month",
        "currency",
        "banner",
        "status",
        "meeting_rom_quantity",
        "phonebooth_quantity",
        "amphitheater_quantity",
    ]

    def form_valid(self, form):
        # Call the parent class's form_valid method to save the building instance
        response = super().form_valid(form)

        # Handle the assignment of public IPs
        public_ip_ids = self.request.POST.getlist("public_ips")
        existing_public_ip_ids = list(
            self.object.public_ips.values_list("id", flat=True)
        )

        public_ips_to_add = set(public_ip_ids) - set(existing_public_ip_ids)

        public_ips_to_remove = []
        for ip in existing_public_ip_ids:
            print(ip)
            if str(ip) not in public_ip_ids:
                print(ip)
                public_ips_to_remove.append(ip)

        # Add new public IPs
        for public_ip_id in public_ips_to_add:
            ip = PublicIP.objects.filter(pk=public_ip_id).first()
            ip.building = self.object
            ip.save()

        # Remove existing public IPs
        for public_ip_id in public_ips_to_remove:
            ip = PublicIP.objects.filter(pk=public_ip_id).first()
            self.object.public_ips.remove(ip)

        return response

    def get_success_url(self):
        return reverse_lazy("building_details", kwargs={"pk": self.object.pk})


class BuildingDeleteView(PermissionRequiredMixin, DeleteView):
    model = Building
    template_name = "generic/views/confirm_delete.html"
    extra_context = {"form_action": "delete"}
    permission_required = "building.delete_building"

    def get_success_url(self):
        return reverse_lazy("building_list")


class BuildingSearchView(LoginRequiredMixin, ListView):
    model = Building
    context_object_name = "instance_object"
    template_name = "building/buildings.html"
    extra_context = {
        "custom_title": "Buildings",
        "create_url": "building_creation",
        "update_url": "building_update",
        "details_url": "building_details",
        "delete_url": "building_delete",
        "export_url": "building_export",
        "list_url": "building_list",
    }

    def get_queryset(self):
        query = self.request.GET.get("query")
        object_list = Building.objects.filter(Q(name__icontains=query))
        return object_list

    def get_success_url(self):
        return reverse_lazy(
            "building_list", kwargs={"query": self.request.GET.get("query")}
        )


class BuildingResource(resources.ModelResource):
    class Meta:
        model = Building


class BuildingExport(PermissionRequiredMixin, ViewExport):
    filename = "building.csv"
    content_type = "csv"
    resource = BuildingResource
    permission_required = "service.export_building"


class BuildingPresenceStatisticView(PermissionRequiredMixin, ListView):
    template_name = "building/building_presence_statistics.html"
    permission_required = "building.view_accesscontrolrecord"
    context_object_name = "instance_object"
    extra_context = {
        "custom_title": "Statistics",
        "export_url": "export_building",
        "button_create_permission": True,
    }

    def get_queryset(self, *args, **kwargs):
        pk = self.kwargs["pk"]
        raw_query = f"""SELECT YEAR(date) AS year, WEEK(date) as week_num,
            DATE(date) AS date_value,user_type as type, count(DISTINCT(card)) AS count_entry 
            FROM `building_accesscontrolrecord` 
            WHERE building_id={pk} 
            GROUP BY year, week_num, date_value, type
            ORDER BY date_value DESC;"""
        cursor = connection.cursor()
        cursor.execute(raw_query)
        object_list = dictfetchall(cursor)
        return object_list

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        pk = self.kwargs["pk"]
        context["building_pk"] = pk
        return context


class BuildingDocumentCreateView(PermissionRequiredMixin, View):
    form_class = UploadBuildingDocumentFrom
    permission_required = "building.add_document"

    def post(self, request, pk):
        form = self.form_class(request.POST, request.FILES)

        if form.is_valid():
            filename = request.FILES["file"].name
            form.instance.name = filename
            form.instance.createdByUserId = self.request.user
            form.instance.lpk = pk
            form.instance.set_upload_folder("building")
            result = form.save()
            contract = get_object_or_404(Building, pk=pk)
            contract.document.add(result.id)

        tab = self.request.GET.get("tab")
        path = reverse_lazy("building_details", kwargs={"pk": pk})
        tab = self.request.GET.get("tab", "document")
        url = f"{path}?tab={tab}"

        return HttpResponseRedirect(url)


class BuildingDocumentRemoveView(PermissionRequiredMixin, View):
    permission_required = "building.delete_document"

    def post(self, request, pk, lpk):
        request.POST.get("relation")
        curent_object = get_object_or_404(Building, pk=pk)
        curent_object.document.remove(lpk)

        link_object = get_object_or_404(ContractDocument, pk=lpk)
        link_object.delete()
        tab = self.request.GET.get("tab")
        path = reverse_lazy("building_details", kwargs={"pk": pk})
        tab = self.request.GET.get("tab", "document")
        url = f"{path}?tab={tab}"

        return HttpResponseRedirect(url)
