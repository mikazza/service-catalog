from django import template
from service.models import (
    User,
)
from building.models import Building, PublicIP, Country

register = template.Library()


@register.simple_tag
def get_user_building(building_id: int, is_active: bool = True):
    building = Building.objects.filter(pk=building_id).first()

    return User.objects.filter(building_id=building, is_active=is_active).all().count()


@register.simple_tag
def get_total_building():
    objects = Building.objects.all()
    return objects.count()


@register.simple_tag
def get_pesence_target(building_id: int):
    target = 1
    building = Building.objects.filter(pk=building_id).first()

    if building.target_presence:
        target = building.target_presence
    return target


@register.simple_tag
def get_all_building():
    objects = Building.objects.all()
    return objects


@register.simple_tag
def get_active_users():
    objects = User.objects.filter(is_active=True).all()
    return objects


@register.filter
def divide(value, arg):
    try:
        if value and arg:
            return float(value) / float(arg)
        return 0
    except (ValueError, ZeroDivisionError):
        return None


@register.simple_tag
def get_all_public_ip():
    return PublicIP.objects.all()


@register.simple_tag
def get_all_country():
    return Country.objects.all()
