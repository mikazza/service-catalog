// https://labpys.com/how-to-create-cascading-dependent-dropdown-list-in-django-python/

$(document).ready(function(){
 
    var department = $("#id_department");
    var subDepartment = $("#id_subdepartment");
    var $options = subDepartment.find('option');
    department.on('change',function(){
      subDepartment.html($options.filter('[data-dep-id="'+ this.value  +'"]'));
    }).trigger('change'); 
    
   });