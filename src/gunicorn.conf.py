# Gunicorn configuration file
# https://docs.gunicorn.org/en/stable/configure.html#configuration-file
# https://docs.gunicorn.org/en/stable/settings.html

from environs import Env

env = Env()
env.read_env()
PORT = env.int("PORT", 8000)
SERVER_BIND_IP = env.str("SERVER_BIND_IP", "localhost")

bind = f"{SERVER_BIND_IP}:{PORT}"
workers = env.int("GUNICORN_WORKER", 1)
log_file = "-"
loglevel = env.str("LOGLEVEL", "info").lower()
wsgi_app = "core.wsgi:application"
