"""
Django settings for service_catalog project.

Generated by 'django-admin startproject' using Django 4.0.

For more information on this file, see
https://docs.djangoproject.com/en/4.0/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/4.0/ref/settings/
"""

from pathlib import Path
import os
from django.core.management import utils
import django_heroku
import django
from django.utils.encoding import force_str
from environs import Env
from marshmallow.validate import OneOf, Email
from django.utils.log import DEFAULT_LOGGING
import logging.config
import logging

# https://pypi.org/project/environs/
env = Env()
env.read_env()

django.utils.encoding.force_text = force_str

# Disable Django's logging setup
LOGGING_CONFIG = None

# Login configuration
LOGLEVEL = env.str("LOGLEVEL", "info").upper()

logging.config.dictConfig(
    {
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": {
            "default": {
                # exact format is not important, this is the minimum information
                "format": "%(asctime)s %(name)-12s %(levelname)-8s %(message)s",
            },
            "django.server": DEFAULT_LOGGING["formatters"]["django.server"],
        },
        "handlers": {
            # console logs to stderr
            "console": {
                "class": "logging.StreamHandler",
                "formatter": "default",
            },
            "django.server": DEFAULT_LOGGING["handlers"]["django.server"],
        },
        "root": {
            "handlers": ["console"],
            "level": "INFO",
        },
        "loggers": {
            # default for all undefined Python modules
            "": {
                "level": "INFO",
                "handlers": ["console"],
            },
            # Our application code
            "app": {
                "level": LOGLEVEL,
                "handlers": ["console"],
                # Avoid double logging because of root logger
                "propagate": False,
            },
            # Default runserver request logging
            "django.server": DEFAULT_LOGGING["loggers"]["django.server"],
        },
    }
)

logger = logging.getLogger(__name__)


# App version

APP_VERSION = env.str("APP_VERSION", "Unknown")
ENV = env.str(
    "ENV",
    validate=OneOf(["prod", "dev"], error="ENV must be one of: {choices}"),
    default="dev",
)
logger.info(f"ENV: {ENV}")
logger.info(f"APP_VERSION: {APP_VERSION}")

# JWT configuration
SIMPLE_JWT = {
    "USER_ID_FIELD": "id",
    "USER_ID_CLAIM": "user_id",
}

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent
logger.info(f"BASE_DIR: {BASE_DIR}")


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/4.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env.str("DJANO_SECRET_KEY", utils.get_random_secret_key())

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = env.bool("DJANGO_DEBUG", False)

PORT = env.int("PORT", 8000)
ALLOWED_HOSTS = env.list("ALLOWED_HOSTS", ["*"])
CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_CREDENTIALS = True
CSRF_TRUSTED_ORIGINS = env.list("CSRF_TRUSTED_ORIGINS", [f"http://localhost:{PORT}"])
CORS_ALLOWED_ORIGINS = env.list("CORS_ALLOWED_ORIGINS", [f"http://localhost:{PORT}"])
CORS_ORIGIN_WHITELIST = env.list("CORS_ORIGIN_WHITELIST", [f"http://localhost:{PORT}"])

logger.info(f"CORS_ALLOWED_ORIGINS: {CORS_ALLOWED_ORIGINS}")
logger.info(f"CORS_ORIGIN_WHITELIST: {CORS_ORIGIN_WHITELIST}")
logger.info(f"CSRF_TRUSTED_ORIGINS: {CSRF_TRUSTED_ORIGINS}")

EMAIL_USE_TLS = True
EMAIL_BACKEND = env.str("EMAIL_BACKEND", "django.core.mail.backends.smtp.EmailBackend")
EMAIL_HOST = env.str("EMAIL_HOST", None)
EMAIL_HOST_USER = env.str("EMAIL_HOST_USER", None)
EMAIL_HOST_PASSWORD = env.str("EMAIL_HOST_PASSWORD", None)
EMAIL_PORT = env.int("EMAIL_PORT", 587)

# Application definition

INSTALLED_APPS = [
    "whitenoise.runserver_nostatic",
    "django.contrib.staticfiles",
    "dal",
    "dal_select2",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.sites",
    "django_filters",
    "rest_framework",
    "service.apps.ServiceConfig",
    "drf_yasg",
    "storages",
    "corsheaders",
    "graphene_django",
    "allauth",
    "allauth.account",
    "allauth.socialaccount",
    "allauth.socialaccount.providers.microsoft",
    "allauth.socialaccount.providers.google",
    "allauth.socialaccount.providers.openid",
    "allauth.socialaccount.providers.okta",
    "allauth.socialaccount.providers.gitlab",
    "allauth.socialaccount.providers.github",
    "graphql_jwt.refresh_token.apps.RefreshTokenConfig",
    "rest_framework_simplejwt",
    "health_check",
    "health_check.db",
    "health_check.contrib.migrations",
    "health_check.cache",
    "mozilla_django_oidc",
    "django_extensions",
    "oauth2_provider",
    "rest_framework.authtoken",
    "import_export",
    "django.contrib.humanize",
    "scim.apps.ScimConfig",
    "hardware.apps.HardwareConfig",
    "restapi.apps.RestapiConfig",
    "admin.apps.AdminConfig",
    "gql.apps.GqlConfig",
    "application.apps.ApplicationConfig",
    "django_celery_beat",
    "django_celery_results",
    "building.apps.BuildingConfig",
    "dbbackup",
]

MIDDLEWARE = [
    "service.middleware.healthcheck.HealthCheckMiddleware",
    "django.middleware.security.SecurityMiddleware",
    "whitenoise.middleware.WhiteNoiseMiddleware",
    "corsheaders.middleware.CorsMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "oauth2_provider.middleware.OAuth2TokenMiddleware",
    "allauth.account.middleware.AccountMiddleware",
]

ROOT_URLCONF = "core.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [
            os.path.join(BASE_DIR, "templates"),
        ],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

WSGI_APPLICATION = "core.wsgi.application"


# Database
# https://docs.djangoproject.com/en/4.0/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": env.str("DB_DRIVER", "django.db.backends.mysql"),
        "NAME": env.str("DB_NAME", "catalog"),
        "USER": env.str("DB_USER", "catalog"),
        "PASSWORD": env.str("DB_PASSWD", "catalog"),
        "HOST": env.str("DB_SERVER", "127.0.0.1"),
        "PORT": env.str("DB_PORT", "3306"),
        "OPTIONS": {
            "ssl": env.bool("DB_SSL", False),
            "connect_timeout": env.int("DB_CONNECTION_TIEMOUT", 3),
        },
    },
    # https://learn.microsoft.com/en-us/samples/azure-samples/mssql-django-samples/mssql-django-samples/
    "acccess_control": {
        "ENGINE": env.str("AC_DB_DRIVER", "mssql"),
        "NAME": env.str("AC_DB_NAME", "access_control"),
        "USER": env.str("AC_DB_USER", "service_catalog"),
        "PASSWORD": env.str("AC_DB_PASSWD", None),
        "HOST": env.str("AC_DB_SERVER", "127.0.0.1"),
        "PORT": env.str("AC_DB_PORT", "1433"),
        "OPTIONS": {
            "driver": env.str(
                "AC_DB_ODBC_DRIVER_VERSION", "ODBC Driver 17 for SQL Server"
            ),
            "unicode_results": env.bool("AC_DB_UNICODE_RESULT", True),
            "trust_certificate": env.bool("AC_DB_TRUST_CERTIFICATE", True),
            "connection_timeout": env.int("AC_DB_CONNECTION_TIMEOUT", 1),
            "connection_retries": env.int("AC_DB_CONNECTION_RETRIES", 5),
        },
    },
}


# Password validation
# https://docs.djangoproject.com/en/4.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]

AUTHENTICATION_BACKENDS = [
    "django.contrib.auth.backends.ModelBackend",
    "allauth.account.auth_backends.AuthenticationBackend",
    "graphql_jwt.backends.JSONWebTokenBackend",
    "oauth2_provider.backends.OAuth2Backend",
]

SITE_ID = 1
AUTH_USER_MODEL = "service.User"


ACCOUNT_FORMS = {
    "login": "allauth.account.forms.LoginForm",
    "signup": "allauth.account.forms.SignupForm",
    "add_email": "allauth.account.forms.AddEmailForm",
    "change_password": "allauth.account.forms.ChangePasswordForm",
    "set_password": "allauth.account.forms.SetPasswordForm",
    "reset_password": "allauth.account.forms.ResetPasswordForm",
    "reset_password_from_key": "allauth.account.forms.ResetPasswordKeyForm",
    "disconnect": "allauth.socialaccount.forms.DisconnectForm",
}

SOCIALACCOUNT_FORMS = {
    "disconnect": "allauth.socialaccount.forms.DisconnectForm",
    "signup": "allauth.socialaccount.forms.SignupForm",
}

# ALLAUTH config
# https://django-allauth.readthedocs.io/en/latest/providers.html#okta

ACCOUNT_USER_MODEL_USERNAME_FIELD = None
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_USERNAME_REQUIRED = False
ACCOUNT_AUTHENTICATION_METHOD = "email"
ACCOUNT_DEFAULT_HTTP_PROTOCOL = env.str("ACCOUNT_DEFAULT_HTTP_PROTOCOL", "http")
ACCOUNT_EMAIL_VERIFICATION = env.str("ACCOUNT_EMAIL_VERIFICATION", "none")
SOCIALACCOUNT_AUTO_SIGNUP = True
LOGIN_REDIRECT_URL = "/service/"
LOGOUT_REDIRECT_URL = "/service/"

SOCIALACCOUNT_PROVIDERS = {
    "microsoft": {
        "tenant": env.str("SL_AZURE_TENANT_ID", None),
    },
    "google": {
        "SCOPE": [
            "profile",
            "email",
        ],
        "AUTH_PARAMS": {
            "access_type": "online",
        },
    },
    "keycloak": {
        "KEYCLOAK_URL": env.str("SL_KEYCLOAK_URL", None),
        "KEYCLOAK_REALM": env.str("SL_KEYCLOAK_REALM", None),
    },
    "okta": {
        "OKTA_BASE_URL": env.str("SL_OKTA_BASE_URL", None),
    },
    "gitlab": {
        "GITLAB_URL": env.str("SL_GITLAB_URL", "https://gitlab.com/"),
        "SCOPE": env.list("SL_SCOPE", ["api"]),
    },
    "github": {
        "SCOPE": [
            "user",
            "repo",
            "read:org",
        ],
    },
    "openid": {
        "SERVERS": [
            dict(
                id=env.str("SL_OPENID_SERVER_ID", "default"),
                name=env.str("SL_OPENID_SERVER_NAME", "default"),
                openid_url=env.str(
                    "SL_OPENID_SERVER_URL", "http://localhost/endpoint/"
                ),
            )
        ],
    },
}

# Internationalization
# https://docs.djangoproject.com/en/4.0/topics/i18n/

LANGUAGE_CODE = "en-us"
TIME_ZONE = "UTC"
USE_I18N = True
USE_TZ = True

# Default primary key field type
# https://docs.djangoproject.com/en/4.0/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

REST_FRAMEWORK = {
    "DEFAULT_PAGINATION_CLASS": "rest_framework.pagination.PageNumberPagination",
    "PAGE_SIZE": 100,
    "DEFAULT_AUTHENTICATION_CLASSES": (
        "rest_framework.authentication.SessionAuthentication",
        "rest_framework_simplejwt.authentication.JWTAuthentication",
        "oauth2_provider.contrib.rest_framework.OAuth2Authentication",
        "rest_framework.authentication.TokenAuthentication",
    ),
    "DEFAULT_PERMISSION_CLASSES": (
        "restapi.permissions.IsStaffUserWithDjangoModelPermissions",
    ),
}

STATICFILES_FINDERS = [
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
]


# S3 storage
# https://www.section.io/engineering-education/how-to-upload-files-to-aws-s3-using-django-rest-framework/
# https://testdriven.io/blog/storing-django-static-and-media-files-on-amazon-s3/
# https://simpleisbetterthancomplex.com/tutorial/2017/08/01/how-to-setup-amazon-s3-in-a-django-project.html

USE_AWS_S3_FOR_FILES = env.bool("USE_AWS_S3_FOR_FILES", False)

if USE_AWS_S3_FOR_FILES:
    logger.info("S3 storage will be used for statics file")
    AWS_ACCESS_KEY_ID = env.str("AWS_ACCESS_KEY_ID")
    AWS_SECRET_ACCESS_KEY = env.str("AWS_ACCESS_KEY_SECRET")
    AWS_STORAGE_BUCKET_NAME = env.str("AWS_S3_BUCKET_NAME")
    AWS_S3_CUSTOM_DOMAIN = env.str(
        "AWS_S3_CUSTOM_DOMAIN", f"{AWS_STORAGE_BUCKET_NAME}.s3.amazonaws.com"
    )
    AWS_S3_OBJECT_PARAMETERS = {"CacheControl": "max-age=86400"}
    AWS_S3_SIGNATURE_VERSION = env.str("AWS_S3_SIGNATURE_VERSION", "s3v4")
    AWS_S3_REGION_NAME = env.str("AWS_S3_REGION_NAME", "eu-west-3")
    AWS_S3_FILE_OVERWRITE = env.bool("AWS_S3_FILE_OVERWRITE", False)
    AWS_DEFAULT_ACL = env.str("AWS_DEFAULT_ACL", None)
    AWS_S3_VERIFY = env.bool("AWS_S3_VERIFY", True)
    AWS_STATIC_LOCATION = env.str("AWS_STATIC_LOCATION", "static")
    STATIC_URL = f"https://{AWS_S3_CUSTOM_DOMAIN}/{AWS_STATIC_LOCATION }/"
    STATICFILES_STORAGE = "service.storages.AWSStaticStorage"
    AWS_PUBLIC_MEDIA_LOCATION = env.str("AWS_PUBLIC_MEDIA_LOCATION", "media/public")
    DEFAULT_FILE_STORAGE = "service.storages.AWSPublicMediaStorage"
    AWS_PRIVATE_MEDIA_LOCATION = env.str("AWS_PRIVATE_MEDIA_LOCATION", "media/private")
    PRIVATE_MEDIA_LOCATION = env.str("AWS_PRIVATE_MEDIA_LOCATION", "media/private")
    PRIVATE_FILE_STORAGE = "service.storages.AWSPrivateMediaStorage"
    MEDIA_URL = f"https://{AWS_S3_CUSTOM_DOMAIN}/{AWS_PUBLIC_MEDIA_LOCATION }/"
    DBBACKUP_STORAGE = PRIVATE_FILE_STORAGE
    DBBACKUP_STORAGE_OPTIONS = {
        "location": os.path.join(PRIVATE_MEDIA_LOCATION, "database/backup/")
    }


else:
    logger.info("Local storage will be used for statics file")
    STATIC_URL = env.str("STATIC_URL", "static/")
    STATIC_ROOT = os.path.join(BASE_DIR, "staticfiles")
    MEDIA_URL = env.str("MEDIA_URL", "mediafiles/")
    MEDIA_ROOT = os.path.join(BASE_DIR, "mediafiles")
    AWS_STATIC_LOCATION = STATIC_URL
    AWS_PUBLIC_MEDIA_LOCATION = "media/public"
    AWS_PRIVATE_MEDIA_LOCATION = "media/private"
    DEFAULT_FILE_STORAGE = "django.core.files.storage.FileSystemStorage"
    STATICFILES_STORAGE = "whitenoise.storage.CompressedStaticFilesStorage"
    DBBACKUP_STORAGE = DEFAULT_FILE_STORAGE
    DBBACKUP_STORAGE_OPTIONS = {
        "location": os.path.join(MEDIA_ROOT, "private/database/backup/")
    }
STATICFILES_DIRS = [os.path.join(BASE_DIR, "static")]


# SWAGGER configuration
SWAGGER_SETTINGS = {
    "USE_SESSION_AUTH": True,
    "LOGOUT_URL": "/accounts/logout",
    "LOGIN_URL": "/accounts/login",
    "SECURITY_DEFINITIONS": {
        "Bearer": {
            "type": "apiKey",
            "name": "Authorization",
            "in": "header",
            "description": "Bearer token",
        },
        "Basic": {"type": "basic"},
    },
}

# GraphQL
GRAPHENE = {
    "SCHEMA": "service.schema.schema",
    "ATOMIC_MUTATIONS": True,
    "MIDDLEWARE": [
        "graphql_jwt.middleware.JSONWebTokenMiddleware",
    ],
}

GRAPHQL_JWT = {
    "JWT_VERIFY_EXPIRATION": True,
    "JWT_LONG_RUNNING_REFRESH_TOKEN": True,
    "JWT_ALLOW_ARGUMENT": True,
}

DEFAULT_PAGINATION = env.int("DEFAULT_PAGINATION", 24)


# OIDC configuration
OIDC_ACTIVATED = env.bool("OIDC_ACTIVATED", False)

if OIDC_ACTIVATED:
    AUTHENTICATION_BACKENDS.append("service.oidc.CustomOIDCAB")
    OIDC_RP_CLIENT_ID = env.str("OIDC_RP_CLIENT_ID", None)
    OIDC_RP_CLIENT_SECRET = env.str("OIDC_RP_CLIENT_SECRET", None)
    OIDC_OP_AUTHORIZATION_ENDPOINT = env.str("OIDC_OP_AUTHORIZATION_ENDPOINT", None)
    OIDC_OP_TOKEN_ENDPOINT = env.str("OIDC_OP_TOKEN_ENDPOINT", None)
    OIDC_OP_USER_ENDPOINT = env.str("OIDC_OP_USER_ENDPOINT", None)
    OIDC_RP_SIGN_ALGO = env.str("OIDC_RP_SIGN_ALGO", "RS256")
    OIDC_OP_JWKS_ENDPOINT = env.str("OIDC_OP_JWKS_ENDPOINT", None)
    OIDC_CREATE_USER = env.bool("OIDC_CREATE_USER", False)
    SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")
    SECURE_SSL_REDIRECT = env.bool("SECURE_SSL_REDIRECT", True)

    logger.info(f"OIDC_ACTIVATED: {OIDC_ACTIVATED}")
    logger.info(f"OIDC_OP_AUTHORIZATION_ENDPOINT: {OIDC_OP_AUTHORIZATION_ENDPOINT}")
    logger.info(f"OIDC_OP_TOKEN_ENDPOINT: {OIDC_OP_TOKEN_ENDPOINT}")
    logger.info(f"OIDC_OP_USER_ENDPOINT: {OIDC_OP_USER_ENDPOINT}")
    logger.info(f"OIDC_RP_SIGN_ALGO: {OIDC_RP_SIGN_ALGO}")
    logger.info(f"OIDC_OP_JWKS_ENDPOINT: {OIDC_OP_JWKS_ENDPOINT}")
    logger.info(f"OIDC_CREATE_USER: {OIDC_CREATE_USER}")
    logger.info(f"SECURE_PROXY_SSL_HEADER: {SECURE_PROXY_SSL_HEADER}")
    logger.info(f"SECURE_SSL_REDIRECT: {SECURE_SSL_REDIRECT}")


OAUTH2_PROVIDER = {}

ACTIVATE_DUMP_APP_FROM_IDP = env.bool("ACTIVATE_DUMP_APP_FROM_IDP", False)
IDP_PROVIDER_NAME = env.str(
    "IDP_PROVIDER_NAME",
    validate=OneOf(["onelogin"], error="IDP_PROVIDER_NAME must be one of: {choices}"),
    default="onelogin",
)
if ACTIVATE_DUMP_APP_FROM_IDP:
    IDP_CLIENT_ID = env.str("IDP_CLIENT_ID", None)
    IDP_CLIENT_SECRET = env.str("IDP_CLIENT_SECRET", None)
    IDP_ROOT_URL = env.url("IDP_ROOT_URL", None)
    logger.info(f"IDP_ROOT_URL: {IDP_ROOT_URL}")


# Start server command
DJANGO_EXEC_MAKEMIGRATIONS = env.bool("DJANGO_EXEC_MAKEMIGRATIONS", False)
DJANGO_EXEC_MIGRATE = env.bool("DJANGO_EXEC_MIGRATE", False)
DJANGO_EXEC_CREATESUPERUSER = env.bool("DJANGO_EXEC_CREATESUPERUSER", False)
DJANGO_EXEC_COLLECTSTATIC = env.bool("DJANGO_EXEC_COLLECTSTATIC", False)
USE_GUNICORN = env.bool("USE_GUNICORN", False)
DJANGO_SUPERUSER_EMAIL = env.str(
    "DJANGO_SUPERUSER_EMAIL", validate=[Email()], default="admin@tlnk.fr"
)

BIND_DEFAULT = "localhost"
if ENV == "prod":
    BIND_DEFAULT = "0.0.0.0"
SERVER_BIND_IP = env.str("SERVER_BIND_IP", BIND_DEFAULT)
logger.info(f"SERVER_BIND_IP: {SERVER_BIND_IP}")

# Schedule task with celery
CELERY_RESULT_BACKEND = env.str("CELERY_RESULT_BACKEND", "django-db")
CELERY_BROKER_URL = env.str("CELERY_BROKER_URL", "redis://localhost:6379/0")
CELERY_CACHE_BACKEND = env.str("CELERY_CACHE_BACKEND", "default")

CELERY_ACCEPT_CONTENT = ["application/json"]
CELERY_TASK_SERIALIZER = "json"
CELERY_RESULT_SERIALIZER = "json"

CELERYBEAT_SCHEDULER = env.str(
    "CELERYBEAT_SCHEDULER", "djcelery.schedulers.DatabaseScheduler"
)
CELERY_BEAT_SCHEDULER = env.str(
    "CELERY_BEAT_SCHEDULER", "django_celery_beat.schedulers:DatabaseScheduler"
)
CELERY_RESULT_EXTENDED = env.bool("CELERY_RESULT_EXTENDED", True)
CELERY_TIMEZONE = "UTC"

LOAD_HEROKU_CONFIG = env.bool("LOAD_HEROKU_CONFIG", False)
logger.info(f"LOAD_HEROKU_CONFIG: {LOAD_HEROKU_CONFIG}")


if LOAD_HEROKU_CONFIG:
    # activate django heroku
    HEROKU_USE_STATICFILES = env.bool("HEROKU_USE_STATICFILES", False)
    django_heroku.settings(locals(), staticfiles=HEROKU_USE_STATICFILES)
