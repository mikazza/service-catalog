from service.models import Configuration
from django.conf import settings
from django.http import HttpResponse

from django.views.generic import View


def get_pagination_configuration(key: str):
    configuration = None
    pagination = settings.DEFAULT_PAGINATION

    try:
        configuration = Configuration.objects.filter(key=key).first()
        if configuration.enabled:
            pagination = configuration.value

    except Exception:
        pass
    return pagination


class ViewExport(View):
    filename = "filename.csv"
    content_type = "csv"
    resource = None

    def get(self, *args, **kwargs):
        dataset = self.resource().export()
        response = HttpResponse(dataset.csv, content_type=self.content_type)
        response["Content-Disposition"] = f"attachment; filename={self.filename}"
        return response


def is_ajax(request):
    return request.headers.get("x-requested-with") == "XMLHttpRequest"


def dictfetchall(cursor):
    desc = cursor.description
    return [dict(zip([col[0] for col in desc], row)) for row in cursor.fetchall()]
