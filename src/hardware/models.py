from django.db import models
from django.conf import settings
from service.storages import AWSPrivateMediaStorage
from service.models import Provider, Pricing, Contract, Department, SubDepartment, Title
from django.core.files.storage import default_storage
from django.utils.translation import gettext_lazy as _
import os


class HardwareCategory(models.Model):
    name = models.CharField(max_length=255, blank=False, null=True)
    createdByUserId = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        auto_created=True,
        related_name="FK_hardware_category_created_by_user_id",
    )
    creationDate = models.DateTimeField(auto_now_add=True, auto_now=False, blank=True)

    class Meta:
        verbose_name = _("hardware category")
        verbose_name_plural = _("hardware categories")
        ordering = ["id"]
        permissions = [
            ("export_hardware_catagory", "Can export hardware catagory"),
        ]

    def __str__(self):
        return self.name


class Hardware(models.Model):
    STATUS = (
        ("active", "Active"),
        ("decommission", "Decommission"),
    )

    def get_upload_path(instance, filename):
        name, ext = os.path.splitext(filename)
        return os.path.join("hardwares", str(instance.id), name + ext)

    def get_storage():
        storage = default_storage
        if settings.USE_AWS_S3_FOR_FILES:
            storage = AWSPrivateMediaStorage()
        return storage

    name = models.CharField(max_length=255, blank=False, null=True)
    description = models.CharField(max_length=5000, blank=False, null=False)
    provider = models.ForeignKey(
        Provider, on_delete=models.CASCADE, blank=True, null=True, auto_created=False
    )
    status = models.CharField(max_length=30, choices=STATUS, default="active")
    category = models.ForeignKey(
        HardwareCategory,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        auto_created=False,
    )
    model_name = models.CharField(max_length=255, blank=True, null=True)
    model_number = models.CharField(max_length=255, blank=True, null=True)
    contract = models.ManyToManyField(Contract, blank=True)
    icon = models.ImageField(
        upload_to=get_upload_path, null=True, blank=True, storage=get_storage()
    )
    pricing = models.ManyToManyField(Pricing, blank=True)
    support_url = models.CharField(max_length=255, blank=True, null=True)
    createdByUserId = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        auto_created=True,
        related_name="FK_hardware_created_by_user_id",
    )
    creationDate = models.DateTimeField(auto_now_add=True, auto_now=False, blank=True)

    class Meta:
        verbose_name = _("hardware")
        verbose_name_plural = _("hardwares")
        ordering = ["id"]
        permissions = [
            ("export_hardware", "Can export hardware"),
        ]

    def __str__(self):
        return self.name


class RuleMapping(models.Model):
    department = models.ForeignKey(
        Department,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
    )
    subdepartment = models.ForeignKey(
        SubDepartment,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
    )
    title = models.ForeignKey(
        Title,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
    )

    createdByUserId = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        auto_created=True,
        related_name="FK_rule_mapping_created_by_user_id",
    )
    creationDate = models.DateTimeField(auto_now_add=True, auto_now=False, blank=True)

    class Meta:
        unique_together = ("department", "subdepartment", "title")
        verbose_name = _("rule mapping")
        verbose_name_plural = _("rules mapping")
        ordering = ["id"]
        permissions = [
            ("export_rule_mapping", "Can export rule mapping"),
        ]

    def __str__(self):
        return str(f"{self.department}/{self.subdepartment}/{self.title}")


class HardwareMapping(models.Model):
    rule = models.ForeignKey(
        RuleMapping,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
    )

    hardware = models.ManyToManyField(Hardware, blank=True)

    createdByUserId = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        auto_created=True,
        related_name="FK_hardware_mapping_created_by_user_id",
    )
    creationDate = models.DateTimeField(auto_now_add=True, auto_now=False, blank=True)

    class Meta:
        verbose_name = _("hardware mapping")
        verbose_name_plural = _("hardware mapping")
        ordering = ["id"]
        permissions = [
            ("export_hardware_mapping", "Can export hardware mapping"),
        ]

    def __str__(self):
        return str(
            f"{self.rule.department}/{self.rule.subdepartment}/{self.rule.title}"
        )
