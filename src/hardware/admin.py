from django.contrib import admin
from hardware.models import Hardware, HardwareCategory, RuleMapping, HardwareMapping

from import_export.admin import ImportExportModelAdmin
from import_export import resources


class HardwareResource(resources.ModelResource):
    class Meta:
        model = Hardware
        exclude = (
            "contract",
            "pricings" "icon",
        )


class HardwareAdmin(ImportExportModelAdmin):
    resource_class = HardwareResource

    search_fields = ("name",)
    list_display = ("name", "status", "provider", "category", "createdByUserId")
    list_filter = ("provider", "category")
    filter_horizontal = ()


class HardwareCategoryResource(resources.ModelResource):
    class Meta:
        model = HardwareCategory


class HardwareCategoryAdmin(ImportExportModelAdmin):
    resource_class = HardwareCategoryResource

    search_fields = ("name",)
    list_display = ("name", "createdByUserId")
    list_filter = ()
    filter_horizontal = ()


class RuleMappingResource(resources.ModelResource):
    class Meta:
        model = RuleMapping


class RuleMappingAdmin(ImportExportModelAdmin):
    resource_class = RuleMappingResource

    search_fields = ("department", "subdepartment", "title")
    list_display = ("department", "subdepartment", "title")
    list_filter = ()
    filter_horizontal = ()


class HardwareMappingResource(resources.ModelResource):
    class Meta:
        model = HardwareMapping


class HardwareMappingAdmin(ImportExportModelAdmin):
    resource_class = HardwareMappingResource

    search_fields = ("rule", "hardware")
    list_display = ("id", "rule")
    list_filter = ()
    filter_horizontal = ()


admin.site.register(Hardware, HardwareAdmin)
admin.site.register(HardwareCategory, HardwareCategoryAdmin)
admin.site.register(RuleMapping, RuleMappingAdmin)
admin.site.register(HardwareMapping, HardwareMappingAdmin)
