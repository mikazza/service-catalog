from django.urls import path
from scim.views import SCIMGroupViewSet, SCIMUserViewSet


urlpatterns = [
    path(
        "scim/v2/Users",
        SCIMUserViewSet.as_view({"get": "list", "post": "create"}),
        name="scim_user_list",
    ),
    path(
        "scim/v2/Users/<int:pk>",
        SCIMUserViewSet.as_view(
            {"get": "retrieve", "put": "update", "delete": "destroy", "patch": "update"}
        ),
        name="scim_user_list",
    ),
    path(
        "scim/v2/Groups",
        SCIMGroupViewSet.as_view({"get": "list", "post": "create"}),
        name="scim_group_list",
    ),
    path(
        "scim/v2/Groups/<int:pk>",
        SCIMGroupViewSet.as_view(
            {"get": "retrieve", "put": "update", "delete": "destroy", "patch": "update"}
        ),
        name="scim_group_list",
    ),
]
