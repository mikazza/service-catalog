from rest_framework import serializers
from service.models import User
from django.contrib.auth.models import Group
from rest_framework.reverse import reverse
from drf_yasg import openapi


class SCIMGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ["url", "id", "name"]
        swagger_schema_fields = {
            "type": openapi.TYPE_OBJECT,
            "title": "Groups",
            "properties": {
                "schema": openapi.Schema(
                    title="schemas",
                    type=openapi.TYPE_ARRAY,
                    items=openapi.Items(type=openapi.TYPE_STRING),
                ),
                "id": openapi.Schema(
                    title="id",
                    type=openapi.TYPE_INTEGER,
                ),
                "displayName": openapi.Schema(
                    title="displayName",
                    type=openapi.TYPE_STRING,
                ),
                "members": openapi.Schema(
                    title="members",
                    type=openapi.TYPE_ARRAY,
                    items=openapi.Items(type=openapi.TYPE_OBJECT),
                    # additional_properties= {
                    #     "value": openapi.Schema(
                    #         title="value",
                    #         type=openapi.TYPE_INTEGER,
                    #     ),
                    #     "$ref": openapi.Schema(
                    #         title="$ref",
                    #         type=openapi.TYPE_STRING,
                    #     ),
                    #    "display": openapi.Schema(
                    #         title="display",
                    #         type=openapi.TYPE_STRING,
                    #     ),
                    # }
                ),
                "meta": openapi.Schema(
                    title="meta",
                    type=openapi.TYPE_OBJECT,
                    properties={
                        "resourceType": openapi.Schema(
                            title="resourceType",
                            type=openapi.TYPE_STRING,
                        ),
                        "created": openapi.Schema(
                            title="created",
                            type=openapi.TYPE_STRING,
                        ),
                        "lastModified": openapi.Schema(
                            title="lastModified",
                            type=openapi.TYPE_STRING,
                        ),
                        "version": openapi.Schema(
                            title="version",
                            type=openapi.TYPE_STRING,
                        ),
                        "location": openapi.Schema(
                            title="location",
                            type=openapi.TYPE_STRING,
                        ),
                    },
                ),
            },
            "required": ["schema", "displayName"],
        }

    def to_representation(self, instance):
        object = super(SCIMGroupSerializer, self).to_representation(instance)
        group_url = object["url"]
        members = instance.user_set.all()
        group_members = []

        if members:
            for member in members:
                group_members.append(
                    {
                        "value": member.id,
                        "$ref": reverse("scim_user_list", args=[member.id]),
                        "display": member.get_full_name(),
                    }
                )

        result = {
            "schemas": ["urn:ietf:params:scim:schemas:core:2.0:Group"],
            "id": instance.id,
            "displayName": instance.name,
            "members": group_members,
            "meta": {
                "resourceType": "Group",
                "created": "",
                "lastModified": "",
                "version": "",
                "location": group_url,
            },
        }

        return result


class SCIMUserSerializer(serializers.ModelSerializer):
    user_display_name = serializers.ReadOnlyField(source="get_full_name")
    manager_display_name = serializers.ReadOnlyField(source="get_manager_full_name")
    manager_id = serializers.ReadOnlyField(source="get_manager_id")
    department_name = serializers.ReadOnlyField(source="get_department_name")
    subdepartment_name = serializers.ReadOnlyField(source="get_subdepartment_name")
    company_name = serializers.ReadOnlyField(source="get_company_name")
    building_name = serializers.ReadOnlyField(source="get_building_name")

    class Meta:
        model = User
        fields = [
            "url",
            "id",
            "email",
            "first_name",
            "last_name",
            "groups",
            "title",
            "avatar",
            "department",
            "department_name",
            "subdepartment_name",
            "company_name",
            "is_active",
            "date_joined",
            "user_display_name",
            "employeeNumber",
            "manager_display_name",
            "manager_id",
            "building_name",
        ]

        swagger_schema_fields = {
            "type": openapi.TYPE_OBJECT,
            "title": "Groups",
            "properties": {
                "schema": openapi.Schema(
                    title="schemas",
                    type=openapi.TYPE_ARRAY,
                    items=openapi.Items(type=openapi.TYPE_STRING),
                ),
                "id": openapi.Schema(
                    title="id",
                    type=openapi.TYPE_INTEGER,
                ),
                "userName": openapi.Schema(
                    title="displayName",
                    type=openapi.TYPE_STRING,
                ),
                "name": openapi.Schema(
                    title="name",
                    type=openapi.TYPE_OBJECT,
                    properties={
                        "formatted": openapi.Schema(
                            title="formatted",
                            type=openapi.TYPE_STRING,
                        ),
                        "familyName": openapi.Schema(
                            title="familyName",
                            type=openapi.TYPE_STRING,
                        ),
                        "givenName": openapi.Schema(
                            title="givenName",
                            type=openapi.TYPE_STRING,
                        ),
                    },
                ),
                "displayName": openapi.Schema(
                    title="displayName",
                    type=openapi.TYPE_STRING,
                ),
                "emails": openapi.Schema(
                    title="emails",
                    type=openapi.TYPE_ARRAY,
                    items=openapi.Items(type=openapi.TYPE_OBJECT),
                ),
                "title": openapi.Schema(
                    title="title",
                    type=openapi.TYPE_STRING,
                ),
                "active": openapi.Schema(
                    title="active",
                    type=openapi.TYPE_BOOLEAN,
                ),
                "groups": openapi.Schema(
                    title="groups",
                    type=openapi.TYPE_ARRAY,
                    items=openapi.Items(type=openapi.TYPE_OBJECT),
                ),
                "meta": openapi.Schema(
                    title="meta",
                    type=openapi.TYPE_OBJECT,
                    properties={
                        "resourceType": openapi.Schema(
                            title="resourceType",
                            type=openapi.TYPE_STRING,
                        ),
                        "created": openapi.Schema(
                            title="created",
                            type=openapi.TYPE_STRING,
                        ),
                        "lastModified": openapi.Schema(
                            title="lastModified",
                            type=openapi.TYPE_STRING,
                        ),
                        "version": openapi.Schema(
                            title="version",
                            type=openapi.TYPE_STRING,
                        ),
                        "location": openapi.Schema(
                            title="location",
                            type=openapi.TYPE_STRING,
                        ),
                    },
                ),
                "urn:ietf:params:scim:schemas:extension:enterprise:2.0:User": openapi.Schema(
                    title="urn:ietf:params:scim:schemas:extension:enterprise:2.0:User",
                    type=openapi.TYPE_OBJECT,
                    properties={
                        "employeeNumber": openapi.Schema(
                            title="employeeNumber",
                            type=openapi.TYPE_STRING,
                        ),
                        "costCenter": openapi.Schema(
                            title="costCenter",
                            type=openapi.TYPE_STRING,
                        ),
                        "organization": openapi.Schema(
                            title="organization",
                            type=openapi.TYPE_STRING,
                        ),
                        "division": openapi.Schema(
                            title="division",
                            type=openapi.TYPE_STRING,
                        ),
                        "department": openapi.Schema(
                            title="department",
                            type=openapi.TYPE_STRING,
                        ),
                        "manager": openapi.Schema(
                            title="manager",
                            type=openapi.TYPE_OBJECT,
                            properties={
                                "value": openapi.Schema(
                                    title="value",
                                    type=openapi.TYPE_STRING,
                                ),
                                "displayName": openapi.Schema(
                                    title="displayName",
                                    type=openapi.TYPE_STRING,
                                ),
                            },
                        ),
                    },
                ),
                "urn:scim:my:custom:schema": openapi.Schema(
                    title="urn:scim:my:custom:schema",
                    type=openapi.TYPE_OBJECT,
                    properties={
                        "building_id": openapi.Schema(
                            title="building_id",
                            type=openapi.TYPE_STRING,
                        ),
                    },
                ),
            },
            "required": ["schema", "userName"],
        }

    # https://stackoverflow.com/questions/35019030/how-to-return-custom-json-in-django-rest-framework
    # https://www.rfc-editor.org/rfc/rfc7643
    # https://developer.okta.com/docs/reference/scim/scim-20/

    def to_representation(self, instance):
        object = super(SCIMUserSerializer, self).to_representation(instance)
        object_groups = object["groups"]

        groups = []

        if object_groups:
            for g in object_groups:
                group = Group.objects.filter(pk=g).first()
                groups.append({"value": group.id, "display": group.name})

        result = {
            "schemas": [
                "urn:ietf:params:scim:schemas:core:2.0:User",
                "urn:ietf:params:scim:schemas:extension:enterprise:2.0:User",
                "urn:scim:my:custom:schema",
            ],
            "id": str(object["id"]),
            "userName": object["email"],
            "name": {
                "formatted": object["user_display_name"],
                "familyName": object["first_name"],
                "givenName": object["last_name"],
            },
            "displayName": object["user_display_name"],
            "emails": [{"value": object["email"], "primary": True}],
            "title": object["title"],
            "active": object["is_active"],
            "groups": groups,
            "meta": {
                "created": object["date_joined"],
                "lastModified": object["date_joined"],
                "location": object["url"],
            },
            "urn:ietf:params:scim:schemas:extension:enterprise:2.0:User": {
                "employeeNumber": object["employeeNumber"],
                "costCenter": object["department_name"],
                "organization": object["company_name"],
                "division": object["subdepartment_name"],
                "department": object["department_name"],
                "manager": {
                    "value": object["manager_id"],
                    "displayName": object["manager_display_name"],
                },
            },
            "urn:scim:my:custom:schema": {
                "building_id": object["building_name"],
            },
        }

        return result
