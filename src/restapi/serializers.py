from django.contrib.auth.models import Group
from rest_framework import serializers
from service.models import (
    User,
    Service,
    Department,
    SubDepartment,
    IdentityProvider,
    Pricing,
    Provider,
    Application,
    Company,
    Configuration,
    Contact,
    Contract,
    ContractDocument,
    ServiceCategory,
    Title,
)
from hardware.models import Hardware, HardwareCategory, RuleMapping, HardwareMapping
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from building.models import (
    Building,
    AccessControlRecord,
    Controller,
    Currency,
    BuildingSize,
    BuildingType,
    PublicIP,
    ExternalPresenceLogRecord,
    Country,
)


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = [
            "url",
            "id",
            "email",
            "first_name",
            "last_name",
            "groups",
            "avatar",
            "department",
            "subdepartment",
            "manager",
            "company",
            "title",
            "employeeNumber",
            "hardware_mapping",
        ]


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ["url", "id", "name"]


class DepartmentSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Department
        fields = ["url", "id", "name"]


class SubDepartmentSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = SubDepartment
        fields = ["url", "id", "name", "department"]


class TitleSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Title
        fields = ["url", "id", "name"]


class ProviderSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Provider
        fields = [
            "url",
            "id",
            "name",
            "email",
            "phone",
            "vat_number",
            "address",
            "street_number",
            "city",
            "zip_code",
            "country",
            "currency_preffered",
        ]


class ContractSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Contract
        fields = [
            "url",
            "id",
            "name",
            "provider",
            "start_date",
            "end_date",
            "renewal_date",
            "duration",
            "duration_unit",
            "cost_unit",
            "volume",
            "total_cost",
            "status",
            "currency",
            "company",
        ]


class ContractDocumentSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ContractDocument
        fields = [
            "url",
            "id",
            "name",
            "file",
        ]


class PricingSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Pricing
        fields = [
            "url",
            "id",
            "name",
            "cost_unit",
            "total_cost",
            "volume",
            "currency",
            "status",
            "time_unit",
        ]


class CompanySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Company
        fields = [
            "url",
            "id",
            "name",
            "vat_number",
            "invoice_email",
            "address",
            "street_number",
            "city",
            "zip_code",
            "country",
            "brand_name",
            "activity",
            "incorporation_date",
            "share_capital",
            "tax_id",
            "trade_register",
            "shareholders",
        ]


class ServiceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Service
        fields = [
            "url",
            "id",
            "name",
            "description",
            "department",
            "subdepartment",
            "owner",
            "supported_by",
            "service_relationships",
            "tier",
            "provider",
            "status",
            "contract",
            "health_check",
            "icon",
            "pricing",
            "company",
            "service_website",
            "category",
            "identity_provider",
            "service_url",
            "support_url",
            "administrator",
            "procurement",
        ]


class ServiceCategorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ServiceCategory
        fields = [
            "url",
            "id",
            "name",
        ]


class ConfigurationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Configuration
        fields = [
            "url",
            "id",
            "name",
            "description",
            "category",
            "key",
            "value",
            "enabled",
        ]


class IdentityProviderSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = IdentityProvider
        fields = [
            "url",
            "id",
            "name",
            "icon_url",
            "service_url",
        ]


class ContactSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Contact
        fields = ["url", "id", "name", "email", "firstname", "lastname", "phone"]


class CustomTokenObtainPairSerializer(TokenObtainPairSerializer):
    @classmethod
    def get_token(cls, user):
        token = super(CustomTokenObtainPairSerializer, cls).get_token(user)

        # Add custom claims
        token["email"] = user.email
        return token


class TokenObtainPairResponseSerializer(serializers.Serializer):
    access = serializers.CharField()
    refresh = serializers.CharField()

    def create(self, validated_data):
        raise NotImplementedError()

    def update(self, instance, validated_data):
        raise NotImplementedError()


class HardwareCategorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = HardwareCategory
        fields = [
            "url",
            "id",
            "name",
        ]


class HardwareSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Hardware
        fields = [
            "url",
            "id",
            "name",
            "description",
            "provider",
            "status",
            "contract",
            "icon",
            "pricing",
            "category",
            "support_url",
            "model_name",
            "model_number",
        ]


class ApplicationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Application
        fields = [
            "url",
            "id",
            "name",
            "app_id",
            "icon_url",
            "idp",
            "pricing",
            "owner",
            "administrator",
            "provider",
            "status",
            "sso_type",
            "sso_manage_by_provider",
        ]


class RuleMappingSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = RuleMapping
        fields = ["url", "id", "department", "subdepartment", "title"]


class HardwareMappingSerializer(serializers.HyperlinkedModelSerializer):
    rule = RuleMappingSerializer(read_only=True)
    hardware = HardwareSerializer(read_only=True, many=True)
    department = DepartmentSerializer(read_only=True, source="rule.department")
    subdepartment = SubDepartmentSerializer(read_only=True, source="rule.subdepartment")
    title = TitleSerializer(read_only=True, source="rule.title")

    class Meta:
        model = HardwareMapping
        fields = [
            "url",
            "id",
            "rule",
            "department",
            "subdepartment",
            "title",
            "hardware",
        ]


class BuildingTypeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = BuildingType
        fields = ["url", "id", "name"]


class BuildingSizeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = BuildingSize
        fields = ["url", "id", "name"]


class CurrencySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Currency
        fields = ["url", "id", "name"]


class AccessControlRecordSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = AccessControlRecord
        fields = [
            "url",
            "id",
            "source_id",
            "building",
            "date",
            "user",
            "card",
            "controller",
            "user_type",
        ]


class ControllerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Controller
        fields = ["url", "id", "name", "type", "external_id"]


class PublicIPSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = PublicIP
        fields = ["url", "id", "ip"]


class BuildingSerializer(serializers.HyperlinkedModelSerializer):
    public_ips = PublicIPSerializer(many=True)

    class Meta:
        model = Building
        fields = [
            "url",
            "id",
            "name",
            "target_presence",
            "building_manager",
            "it_support",
            "general_manager",
            "culture_embassador",
            "people_director",
            "people_partner",
            "address",
            "street_number",
            "city",
            "zip_code",
            "country",
            "company",
            "capacity",
            "square_meters",
            "reception",
            "type",
            "phone_number",
            "size",
            "renting",
            "start_rending_date",
            "cost_per_month",
            "currency",
            "status",
            "meeting_rom_quantity",
            "phonebooth_quantity",
            "amphitheater_quantity",
            "public_ips",
        ]


class BuildingPublicIpsSerializer(serializers.HyperlinkedModelSerializer):
    public_ips = PublicIPSerializer(many=True)

    class Meta:
        model = Building
        fields = [
            "url",
            "id",
            "name",
            "public_ips",
        ]


class ExternalPresenceLogRecordSerializer(serializers.HyperlinkedModelSerializer):
    user = UserSerializer(read_only=True)
    building = BuildingSerializer(read_only=True)

    class Meta:
        model = ExternalPresenceLogRecord
        fields = [
            "url",
            "id",
            "source_id",
            "source_name",
            "building",
            "date",
            "user",
            "import_date",
        ]


class CountrySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Country
        fields = [
            "url",
            "id",
            "name",
        ]
