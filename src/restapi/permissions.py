from rest_framework.permissions import BasePermission, DjangoModelPermissions


class IsSuperUser(BasePermission):
    """
    Allows access only to authenticated users.
    """

    def has_permission(self, request, view):
        return bool(request.user and request.user.is_superuser)


class IsStaffUserWithDjangoModelPermissions(DjangoModelPermissions):
    """
    Allows access only to authenticated users.
    """

    def has_permission(self, request, view):
        # Workaround to ensure DjangoModelPermissions are not applied
        # to the root view when using DefaultRouter.
        if getattr(view, "_ignore_model_permissions", False):
            return True

        if (
            not request.user
            or (not request.user.is_authenticated and self.authenticated_users_only)
            or not request.user.is_staff
        ):
            return False

        queryset = self._queryset(view)
        perms = self.get_required_permissions(request.method, queryset.model)

        return request.user.has_perms(perms)
