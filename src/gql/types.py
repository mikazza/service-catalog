from graphene_django import DjangoObjectType
from django.contrib.auth.models import Group
from service.models import (
    Service,
    User,
    Department,
    SubDepartment,
    IdentityProvider,
    Company,
    Configuration,
    Contract,
    Contact,
    ContractDocument,
    ServiceCategory,
    Pricing,
    Provider,
    Application,
    Title,
)
from hardware.models import Hardware, HardwareCategory, RuleMapping, HardwareMapping
from building.models import Building, AccessControlRecord, Controller


class UserType(DjangoObjectType):
    class Meta:
        model = User
        fields = "__all__"


class GroupType(DjangoObjectType):
    class Meta:
        model = Group
        fields = "__all__"


class DepartmentType(DjangoObjectType):
    class Meta:
        model = Department
        fields = "__all__"


class SubDepartmentType(DjangoObjectType):
    class Meta:
        model = SubDepartment
        fields = "__all__"


class TitleType(DjangoObjectType):
    class Meta:
        model = Title
        fields = "__all__"


class ProviderType(DjangoObjectType):
    class Meta:
        model = Provider
        fields = "__all__"


class CompanyType(DjangoObjectType):
    class Meta:
        model = Company
        fields = "__all__"


class ContractType(DjangoObjectType):
    class Meta:
        model = Contract
        fields = "__all__"


class ContractDocumentType(DjangoObjectType):
    class Meta:
        model = ContractDocument
        fields = "__all__"


class PricingType(DjangoObjectType):
    class Meta:
        model = Pricing
        fields = "__all__"


class ServiceType(DjangoObjectType):
    class Meta:
        model = Service
        fields = "__all__"


class ServiceCategoryType(DjangoObjectType):
    class Meta:
        model = ServiceCategory
        fields = "__all__"


class ConfigurationType(DjangoObjectType):
    class Meta:
        model = Configuration
        fields = "__all__"


class IdentityProviderType(DjangoObjectType):
    class Meta:
        model = IdentityProvider
        fields = "__all__"


class ContactType(DjangoObjectType):
    class Meta:
        model = Contact
        fields = "__all__"


class HardwareType(DjangoObjectType):
    class Meta:
        model = Hardware
        fields = "__all__"


class HardwareCategoryType(DjangoObjectType):
    class Meta:
        model = HardwareCategory
        fields = "__all__"


class ApplicationType(DjangoObjectType):
    class Meta:
        model = Application
        fields = "__all__"


class RuleMappingType(DjangoObjectType):
    class Meta:
        model = RuleMapping
        fields = "__all__"


class HardwareMappingType(DjangoObjectType):
    class Meta:
        model = HardwareMapping
        fields = "__all__"


class BuildingType(DjangoObjectType):
    class Meta:
        model = Building
        fields = "__all__"


class AccessControlRecordType(DjangoObjectType):
    class Meta:
        model = AccessControlRecord
        fields = "__all__"


class ControllerDeviceType(DjangoObjectType):
    class Meta:
        model = Controller
        fields = "__all__"
