from service.models import User
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = "Switch all user with is_staff=true to false. Super user excluded."

    def handle(self, *args, **options):
        users = User.objects.filter(is_staff=True, is_superuser=False).all()

        self.stdout.write(f"Number of user with is_stall=true: {len(users)}")

        for u in users:
            u.is_staff = False
            u.save()
