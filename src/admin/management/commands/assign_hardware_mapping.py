from django.core.management.base import BaseCommand
from service.models import User
from scim.utils.utils import create_rule_mapping


class Command(BaseCommand):
    help = "Assign hardware mapping object to user profile"

    def handle(self, *args, **options):
        users = User.objects.all()
        count = 0
        count_skip = 0
        count_exist = 0

        for user in users:
            if not user.hardware_mapping:
                if user.department and user.subdepartment and user.title:
                    hardware_mapping = create_rule_mapping(
                        department_id=user.department.id,
                        subdepartment_id=user.subdepartment.id,
                        title_id=user.title.id,
                        current_user=user,
                    )

                    if hardware_mapping:
                        user.hardware_mapping = hardware_mapping
                        user.save()
                        self.stdout.write(
                            f"rule: {hardware_mapping.rule} assigned to {user.email}"
                        )
                        count = count + 1
                    else:
                        count_skip = count_skip + 1
                else:
                    count_skip = count_skip + 1
            else:
                count_exist = count_exist + 1
        self.stdout.write(f"total user update: {str(count)} / {str(len(users))}")
        self.stdout.write(f"total user skip: {str(count_skip)} / {str(len(users))}")
        self.stdout.write(f"total user exist: {str(count_exist)} / {str(len(users))}")
