from service.models import User
from django.core.management.base import BaseCommand
import random
from string import digits, ascii_letters


class Command(BaseCommand):
    help = "Migrate icon url field to field"

    def add_arguments(self, parser):
        parser.add_argument(
            "--number",
            type=int,
            help="Number of accoutn to be created",
        )
        parser.add_argument(
            "--name",
            type=str,
            default="demo",
            help="Name use to genate email",
        )
        parser.add_argument(
            "--domain",
            type=str,
            default="tlnk.fr",
            help="domain used for generating email",
        )
        parser.add_argument(
            "--pwdlength",
            type=int,
            default=20,
            help="Length of the password to be used",
        )

    def handle(self, *args, **options):
        number = options["number"]
        name = options["name"]
        domain = options["domain"]
        password_length = options["pwdlength"]

        self.stdout.write(f"Number of account to create: {number}")
        self.stdout.write(f"Name to use: {name}")
        self.stdout.write(f"Domain to use: {domain}")
        self.stdout.write(f"Password will be random with length: {password_length}")

        def _pw(length=10):
            s = ""

            for i in range(length):
                s += random.choice(digits + ascii_letters)
            return s

        def get_user(email: str):
            return User.objects.filter(email=email).first()

        for num in range(number):
            email = f"{name}-{num}@{domain}"

            if not get_user(email=email):
                user = User.objects.create_user(
                    email=email, password=_pw(password_length)
                )
                user.is_superuser = False
                user.is_staff = False
                user.save()
