from django.core.management.base import BaseCommand
from django.core.management import call_command
from django.conf import settings
from core.settings import logger


class Command(BaseCommand):
    help = "Start server"

    def handle(self, *args, **options):
        if not settings.USE_GUNICORN:
            logger.info("Server will run with poetry")
            call_command("runserver", f"{settings.SERVER_BIND_IP}:{settings.PORT}")
        else:
            logger.info("Server will run with gunicorn")
