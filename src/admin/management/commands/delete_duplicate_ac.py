from django.core.management.base import BaseCommand
from django.db import connection
from core.settings import logger


class Command(BaseCommand):
    help = "Dump access control record from intemo SQL server"

    def handle(self, *args, **options):
        duplicate_raw_query = """SELECT source_id, COUNT(source_id) FROM building_accesscontrolrecord GROUP BY source_id HAVING COUNT(source_id) > 1;"""
        delete_raw_query = f"""DELETE e.* FROM building_accesscontrolrecord e 
        WHERE id IN (SELECT id FROM (SELECT MIN(id) as id FROM building_accesscontrolrecord e2 
        GROUP BY source_id 
        HAVING COUNT(*) > 1) x);"""
        cursor = connection.cursor()
        duplicate = cursor.execute(duplicate_raw_query)
        logger.info(f"Duplicate source_id found:{duplicate}")

        if duplicate > 0:
            logger.info(f"Deleting source_id duplicated:{duplicate}")
            cursor.execute(delete_raw_query)
            logger.info(f"Delete done")
