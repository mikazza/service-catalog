from dal import autocomplete

from django.contrib.auth import get_user_model
from hardware.models import HardwareMapping
from service.models import Application

User = get_user_model()


class ManagerAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        if not self.request.user.is_superuser:
            return User.objects.none()

        qs = User.objects.all()

        if self.q:
            qs = qs.filter(email__istartswith=self.q)

        return qs


class HardwareMappingAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        if not self.request.user.is_superuser:
            return HardwareMapping.objects.none()

        qs = HardwareMapping.objects.all()

        if self.q:
            qs = qs.filter(department__istartswith=self.q)

        return qs


class ApplicationMappingAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        if not self.request.user.is_superuser:
            return Application.objects.none()

        qs = Application.objects.all()

        if self.q:
            qs = qs.filter(department__istartswith=self.q)

        return qs
