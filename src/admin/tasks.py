from django.core.management import call_command
from celery import shared_task
from core.settings import logger
from django.conf import settings


@shared_task
def backup_db():
    if settings.DEBUG is True:
        return "Media Could not be backed up: Debug is True"
    try:
        call_command("dbbackup")
        logger.info("Backed up database successfully")
        return "Backed up database successfully"
    except Exception as e:
        logger.info("Database Could not be backed up:")
        return "Database Could not be backed up"


@shared_task
def backup_media():
    if settings.DEBUG is True:
        return "Media Could not be backed up: Debug is True"
    try:
        call_command("mediabackup")
        logger.info("Backed up media successfully")
        return "Backed up media successfully"
    except Exception as e:
        logger.info("Media Could not be backed up")
        return "Media Could not be backed up"


@shared_task
def test_task():
    return "Test task success"
